var jsdom = require('jsdom');
var configRequire = require('../require.test.json');
require('mock-local-storage');

var require = require('requirejs');
require.config(configRequire);

module.exports = function (template) {

    global.document = jsdom.jsdom();
    global.window = document.defaultView;
    global.window.$ = require('jquery');
    global.navigator = {
      userAgent: 'node.js',
      language: 'en'
    };

    var Page = require('controls');
    var page = new Page();

    page.initialize('demo', {}, "{}", template || '');

    return page;
};