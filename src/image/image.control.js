/**
 * @class Image
 * @classdesc Компонент изображение
 * @extends Control
 *
 * @property name {String} Наименование для разработчика
 * @property url {String} Ссылка для запроса с сервера
 * @property _alt {String} Текст подсказки при отсутствии изображения
 *
 * @author A Ivanov
 */
define("controls.image", [
    "control"
], function (Control) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-image-control",
            data: function () {
                return {
                    _alt: options && options.alt || "Изображение не найдено",
                    url: options && options.url || ""
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});