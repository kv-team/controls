/**
 * @class Control
 * @classdesc Базовый класс для всех компонентов. Данный класс содержит общие методы и свойства компонентов.
 * @extends Ractive
 *
 * @author A Ivanov
 */
define("control", [
    "ractive",
    "jquery",
    "underscore",
    "uuid"
], function (Ractive, $, _, uuid) {
    /**
     * Получение названия компонента
     *
     * @description Должно быть уникальным в рамках конкретного browser
     * Имя должно быть написано на CamelCase и начинаться с типа компонента.
     * Это необходимо для удобства чтения кода в дальнейшем.
     * @return {string} Наименование компонента
     */
    Ractive.prototype.getName = function () {
        return this.get("name");
    };

    /**
     * Получение идентификатора компонента
     *
     * @example kv-control-bba8f871-ff6d-11e6-a1bb-99e908efccb9
     * @return {string}
     */
    Ractive.prototype.getId = function () {
        return this.get("_id");
    };

    /**
     * Получение идентификатора для CSS
     *
     * @example #kv-control-bba8f871-ff6d-11e6-a1bb-99e908efccb9
     * @return {string}
     */
    Ractive.prototype.getCssId = function () {
        return "#" + this.getId();
    };

    /**
     * Название тега компонента
     */
    Ractive.prototype.getComponentName = function () {

        return this.component ? this.component.name : undefined;
    };

    /**
     * Получаем элемент DOM по идентификаторы id. Это как правило подложка компонента.
     */
    Ractive.prototype.getDomInstance = function () {
        return this.jQuery(this.getCssId());
    };

    /**
     * Подписываемся на событие
     * @param eventName
     * @param callback
     */
    Ractive.prototype.subscribe = function (eventName, callback) {
      this.on(eventName, callback);
    };

    return Ractive.extend({
        jQuery: window.jQuery || window.$,
        _: _,
        data: function () {
            return {
                _id: "kv-control-" + uuid.v1(), // Идентификатор
                name: "", // Название
                class: "" // стилевые описания
            }
        },
        oninit: function () {
            var _self = this,
                _browser = _self.findParent("browser");

            if (_browser) {
                _browser.setUI(_self);
            }
        }
    })
});