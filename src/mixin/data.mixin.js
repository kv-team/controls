/**
 * @class Data
 * @classdesc Расширение для работы с items
 *
 * @property _page {Integer} Номер текущей страницы.
 * @property items {Array:json} Массив из данных пришедших с сервера.
 * @property _isLoading {boolean} Признак загрузки данных. Активирует иконку загрузки.
 * @property _infinity_scroll {boolean} Признак разрешающих загрузку по скроллу.
 * @throws Добавьте mixin.filter в компонент
 *
 * @author A Ivanov
 */
define("mixin.data", [], function () {

    return {
        /**
         * Унифицирование пришедших данных с сервера
         */
        _uniqueData: onUniqueData,
        /**
         * Обновление выборки
         */
        refresh: onRefresh,
        /**
         * Очистка выборки
         */
        _clearData: onClearData,
        /**
         * Инициализация обработки загрузки по скроллу. Срабатывает только если стоит флаг _infinity_scroll.
         * @param scroll {Object} Элемент DOM на который вешаем событие scroll
         */
        _initScroll: onInitScroll,
        data: function () {
            return {
                items: []
            }
        },
        oninit: function () {

            var _self = this;
            // Инициализируем основные переменные
            _self.set("_page", 0);
            _self.set("items", []);
            _self.set("_isLoading", false);
            // Подгрузка данных по скроллу
            _self.set("_infinity_scroll", true);

            _self._super();
        }
    };

    function onInitScroll($scroll) {
        var _self = this;
        // Если разрешен подгруз данных (по умолчанию отменен)
        if (_self.get("_infinity_scroll") === true) {

            if ($scroll instanceof Object) {

                $scroll.onscroll = function () {

                    if (event.target.scrollHeight - event.target.scrollTop
                        <= event.target.clientHeight && _self.get("_isOpen") === true) {
                        _self.refresh();
                    }
                }
            }
        }
    }

    function onRefresh() {
        var _self = this;
        var _attr = arguments;

        if (_self.getFiltersValue === undefined) {
            console.warn("Добавьте mixin.filter в компонент " + _self.getComponentName());
            return;
        }

        // Запускаем загрузку если есть url для запроса
        if (_self.get("url")) {
            _self.set("_isLoading", true);
        }

        if (!_self.$get) {
            throw Error("Добавьте mixin.http в компонент " + _self.getComponentName());
        }

        _self.$get({
            page: _self.get("_page"),
            sorts: _self.getSorts ? _self.getSorts() : [],
            filters: _self.getFiltersValue ? _self.getFiltersValue() : {}
        }, function (rows) {

            var _data = _self.get("items"),
                _page = _self.get("_page");

            if (rows && rows.data && rows.data.length > 0) {
                _page++;
            }
            _self.set("_page", _page);

            _data = _data.concat(_self._uniqueData(rows.data));

            // Если требуется вывести информацию строго по определенным колонкам
            // настраивается параметром text строка перечисление через запятую
            if (_attr.length > 0) {
                _data = _data.map(function (row) {
                    var _row = {};
                    _attr[0].split(",").forEach(function(field) {
                        _row[field.trim()] = row[field.trim()];
                    });
                    return _row;
                });
            }

            _self.set("items", _data);

            setTimeout(function () {
                return _self.set("_isLoading", false)
            }, 800);

        });
    }

    function onClearData() {
        this.set("_page", 0);
        this.set("items", []);
    }

    /**
     * Записываем уникальные ID
     * @param data {Array} Данные с сервера
     * @returns {*|Array} Униальный массив данных
     */
    function onUniqueData(data) {

        var _self = this,
            _dataSource = this.get("items"),
            remoteIDs = this._.pluck(data, "id"),
            clientIDs = this._.pluck(_dataSource, "id");

        return _self._.filter(data, function (obj) {
            return _self._.difference(remoteIDs, clientIDs).indexOf(obj.id) >= 0;
        });
    }
});