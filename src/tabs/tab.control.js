/**
 * @class Tab
 * @classdesc Закладка
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.tab", [
    "control"
], function (Control) {

    return function (options) {

        /**
         * Получение названия вкладки
         * @return {string}
         */
        Control.prototype.getCaption = function () {
            return this.get("caption");
        };

        /**
         * Получение индекса вкладки
         * @return {string}
         */
        Control.prototype.getIndex = function () {
            return this.get("index");
        };

        return Control.extend({
            template: "#kv-tabs-control__tab",
            data: function () {
                return {
                    caption: "",
                    index: 0
                };
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});