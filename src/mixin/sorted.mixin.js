/**
 * @class Sorted
 * @classdesc Расширение для работы с сортировкой колонок
 *
 * @property _sorts {Array:String} Содержит значение фильтруемой колонки
 *
 * @author A Ivanov
 */
define("mixin.sorted", [], function () {

        return {
            data: function () {
                return {
                    _sorts: [] // сортируемые колонки
                };
            },
            /**
             * Формируем массив сортируемых колонок
             * @param column {String} Название колонки как в БД
             * @param method {String} desc or asc метод сортировки
             */
            _setSorts: function (column, method) {

                var _sorts = [],
                    _isOk = true;

                for (var idx = 0; idx < _sorts.length; idx++) {
                    if (_sorts[idx][0] === column) {
                        _isOk = false;
                        _sorts[idx][1] = method || "asc";
                        break;
                    }
                }

                if (_isOk === true) {
                    _sorts.push([column, method]);
                }

                this.set("_sorts", _sorts);
            },
            /**
             * Получение всех сортируемых колонок
             * @return {Array:String} Сортируемые колонки
             */
            getSorts: function () {
                return this.get("_sorts") || [];
            },
            onClickColumn: function (event) {

                var $column = this.jQuery(event.node).parents(".column"),
                    _isDesc = $column.hasClass("desc"),
                    _method = !_isDesc ? "desc" : "asc";

                this.jQuery(".column", this.getCssId()).removeClass("desc asc");

                $column.addClass(_method);

                this._setSorts(event.context.field, _method);

                // Если существует метод обнвления данных
                if (this.refresh) {
                    this._clearData();
                    this.refresh();
                }
            },
            oninit: function () {
                var _self = this;

                _self.subscribe({
                    /**
                     * @event clickColumn
                     * @desc Обработка нажатия на заголовок колонки
                     */
                    clickColumn: _self.onClickColumn
                });

                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        }
    });