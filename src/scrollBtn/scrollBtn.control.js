/**
 * @class ScrollBtn
 * @classdesc Кнопка скролла страницы
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.scrollBtn", [
    "control",
    "controls.button"
], function (Control, Button) {

    return function (options) {
      
        return Control.extend({
            isolated: true,
            template: "#kv-scroll-btn-control",
            components: {
                button: Button()
            },
            data: function () {
                return {
                    top_show: 150, /** В каком положении полосы прокрутки начинать показ кнопки "Наверх" */
                    delay: 300 /** Задержка прокрутки */
                }
            },
            oninit: function () {
                var _self = this;

                _self.findComponent("button").subscribe("clickBtn", function () {
                    /** Плавная прокрутка наверх */
                    _self.jQuery("body, html").animate({
                        scrollTop: 0
                    }, _self.get("delay"));
                });

                _self._super();
            },
            onrender: function () {

                var _self = this,
                    $scrollBtn = _self.getDomInstance();

                _self.jQuery(window).scroll(function () {
                    if (_self.jQuery(this).scrollTop() > _self.get("top_show")) {
                        $scrollBtn.fadeIn(_self.get("delay"));
                    } else {
                        $scrollBtn.fadeOut(_self.get("delay"));
                    }
                });

                _self._super();
            }
        });
    };
});