/**
 * @class Frame
 * @classdesc Компонент рамка/подложка
 * @extends Control
 *
 * @property name {String} Название для разработчика
 *
 * @author A Ivanov
 */
define("controls.frame", [
    "control"
], function (Control) {

    return function (options) {

        return Control.extend({
            template: "#kv-frame-control",
            data: function () {
                return {};
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});