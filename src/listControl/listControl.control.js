/**
 * @class listControl
 * @classdesc Список
 *
 * @property items {Array} Массив данных
 *
 * @extends Control
 */
define("controls.listControl", [
    "control",
    "mixin.data",
    "mixin.filter",
    "mixin.http"
], function (Control, DataMixin, FilterMixin, HttpMixin) {
    return function (options) {
        return Control.extend({
            template: "#kv-list-control",
            data: function () {
                return {}
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
                _self.refresh();
            }
        }, DataMixin, FilterMixin, HttpMixin);
    }
});