/**
 * @class Accordion
 * @classdesc Компонент список
 * @extends Control
 * @extend Data
 * @extend Filter
 * @extend Http
 *
 * @property dataSource {Array} Перечень статей
 * @property name {String} Наименование для разработчика
 * @property parent {Integer} Ссылка на родительский элемент
 *
 * @author A Ivanov
 */
define("controls.accordion", [
    "control",
    "mixin.data",
    "mixin.filter",
    "mixin.http",
    "controls.label"
], function (Control, DataMixin, FilterMixin, HttpMixin, Label) {

    return function (options) {

        /**
         * Закрывает все пункты, кроме переданных как параметр
         */
        Control.prototype.closeAllItems = function () {
            var _self = this;

            _self.getDomInstance().find(".kv-accordion-control__item")
                .find(".kv-accordion-control__item").addClass("kv-hidden");
            _self.getDomInstance().find(".kv-accordion-control__item_content").addClass("kv-hidden");
            _self.getDomInstance().find(".kv-active").removeClass("kv-active");
            _self.getDomInstance().find(".kv-accordion-control__item > .glyphicon-menu-up")
                .removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");

        };

        return Control.extend({
            isolated: true,
            template: "#kv-accordion-control",
            partials: {
                item: $("#kv-accordion-control__item").html() || ""
            },
            components: {
                label: Label()
            },
            data: function () {
                return {
                    items: options && options.items || [],
                    text: options && options.text || "name",
                    parent: options && options.parent || null
                }
            },
            oninit: function () {
                var _self = this;

                _self._super();
            },
            onrender: function () {
                var _self = this;

                _self.getDomInstance().find(">.kv-accordion-control__item")
                    .find(".kv-accordion-control__item")
                    .addClass("kv-hidden");

                _self.findAllComponents("label").forEach(function (label) {

                    label.subscribe("labelClick", function (event) {
                        event.original.preventDefault();

                        var $item = label.getDomInstance().parent(".kv-accordion-control__item");

                        if ($item.parent(".kv-accordion-control__item").length === 0) {
                            var $items = _self.getDomInstance().find(">.kv-accordion-control__item").not($item);
                            $items.find(".kv-accordion-control__item").addClass("kv-hidden");
                            $items.find(".kv-accordion-control__item_content").addClass("kv-hidden");
                            _self.getDomInstance().find(".kv-accordion-control__item .kv-label-control").removeClass("kv-active");
                            $items.find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
                        }

                        $item.find(">span").toggleClass("glyphicon-menu-down");
                        $item.find(">span").toggleClass("glyphicon-menu-up");

                        $item.children(".kv-accordion-control__item_content").toggleClass("kv-hidden");
                        $item.find(">.kv-accordion-control__item").toggleClass("kv-hidden");

                        label.getDomInstance().toggleClass("kv-active");
                    })
                });

                _self.jQuery(window).click(function (event) {
                    var $control = _self.jQuery(_self.jQuery(event.target).parents(_self.getCssId())[0]);

                    if ($control.attr("id") !== _self.getId()) {
                        _self.closeAllItems();
                    }
                });

                _self._super();
            }
        }, DataMixin, FilterMixin, HttpMixin);
    }
});