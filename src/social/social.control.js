/**
 * @class SocialButtons
 * @classdesc Компонент кнопки социальных сетей
 * @extends Control
 *
 * @property name {String} Название для разработчика
 * @property isYt {boolean} отображение кнопки Ютуба
 * @property isFB {boolean} отображение кнопки Фейсбука
 * @property isVK {boolean} отображение кнопки Вконтакте
 * @property isTW {boolean} отображение кнопки Твиттер
 * @property isOD {boolean} отображение кнопки Одноклассники
 *
 * @author A Ivanov
 */
define("controls.social", [
    "control",
    "controls.image"
], function (Control, Image) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-social-control",
            components: {
                image: Image()
            },
            data: function () {
                return {
                    isYt: options && options.isYt || true,
                    isFB: options && options.isFB || true,
                    isVK: options && options.isVK || true,
                    isTW: options && options.isTW || true,
                    isOD: options && options.isOD || true
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});