/**
 * @class Utils
 * @classdesc Расширение для работы с переменными
 *
 * @author A Ivanov
 */
define("mixin.utils", ["jquery"], function (jQuery) {

    return {
        /**
         * Преобразует строку с разделителем в массив
         * @param value {String} Перечисление (через запятую)
         * @return {Array}
         */
        stringToArray: function (value) {

            var _result = [];

            if (value === typeof "string") {

                if (value.indexOf(",")) {

                    value.split(",").forEach(function (item) {
                        _result.push(item.trim());
                    });

                } else {
                    _result.push(value.trim());
                }
            }

            return _result;
        },
        /**
         * Удалить объект json в массиве по полю id в объекте
         * @desc Удаляет только первое вхождение
         * @param arr {Array} Массив данных
         * @param id {Integer} Идентификатор по которому нужно удалить
         * @return {Object}
         */
        removeObjToArrayByObjId: function (arr, id) {

            var _result = [];

            if (arr) {
                for (var idx = 0; idx < arr.length; idx++) {
                    if (arr[idx].id === id) {
                        _result = arr.splice(idx, 1);
                        break;
                    }
                }
            }

            return _result;
        },
        /**
         * Получение знчения из JSON по сложному ключу
         * @param src {Object} - JSON в котором ищем
         * @param key {String} - сложный ключ (имеет разделитель ".")
         * @return {Object}
         */
        getValueById: function (src, key) {

            var _prepare = src || {};

            if (key) {
                var _keys = key.split(".");

                _keys.forEach(function (value) {
                    _prepare = _prepare[value];
                });
                return _prepare;
            } else {
                return {};
            }
        },
        /**
         * Объединение локалей в общий файл
         * @param obj1 {Object}
         * @param obj2 {Object}
         * @return {Object}
         */
        mergeJSON: function (obj1, obj2) {

            var result = {};

            obj1 = obj1 || {};
            obj2 = obj2 || {};

            Object.keys(obj1).forEach(function (locale) {
                result[locale] = jQuery.extend({}, obj1[locale], obj2[locale]);
            });

            return result;
        }
    }
});
