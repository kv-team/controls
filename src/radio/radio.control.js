/**
 * @class Radio
 * @classdesc Компонент выборка значения
 * @extends Control
 * @extend Resulted
 *
 * @property _dataSource {Array:json} Перечень значений для компонента {id: 111, name: "dsadad"}
 *
 * @author A Ivanov
 */
define("controls.radio", [
    "control",
    "mixin.resulted"
], function (Control, ResultedMixin) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-radio-control",
            data: function () {
                return {
                    _dataSource: options && options.dataSource || []
                }
            },
            oninit: function () {
                var _self = this;

                /**
                 * Нажатие на позицию списка
                 * @event onClick
                 */
                _self.subscribe("onClick", function (event) {
                    _self.setResult(event.context.id);
                });

                _self._super();
            },
            onrender: function () {
                var _self = this;

                _self.watchResult(function (newVal) {

                    var _radio = _self.nodes[_self.getId() + "_radio_" + newVal];

                    if (_radio !== undefined) {
                        _radio.checked = true;
                    }
                });

                _self._super();
            }
        }, ResultedMixin);
    }
});