/**
 * @class CheckBox
 * @classdesc Компонент панель с выбором
 * @extends Control
 * @extend Resulted
 *
 * @property caption {String} Текст в компоненте
 *
 * @author A Ivanov
 */
define("controls.checkBox", [
    "control",
    "mixin.resulted"
], function (Control, ResultedMixin) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-check-box-control",
            data: function () {
                return {
                    caption: options && options.caption || "Default"
                }
            },
            oninit: function () {
                var _self = this;

                /**
                 * Нажатие на компонент
                 * @event clickCheck
                 */
                _self.subscribe("clickCheck", function (event) {

                    _self.setResult(event.node.checked || null);
                });

                _self._super();
            },
            onrender: function () {
                var _self = this;

                _self.watchResult(function (newVal) {
                    _self.nodes[_self.getId()].checked = newVal || null;
                });

                _self._super();
            }
        }, ResultedMixin);
    }
});