/**
 * @class Carousel slide
 * @classdesc Компонент слайд
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.slide", [
    "control"
], function (Control) {

    return function () {

        return Control.extend({
            template: "#kv-carousel-control__slide",
            partials: {
                slide: _getPartial
            },
            data: function () {
                return {};
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });

        /**
         * Получение шаблона закладки
         * @param options {json} Параметры со страницы при инициализации
         * @return {string} Возвращает шаблон
         */
        function _getPartial(options) {
            var _content = this.get("content");

            if (!_content) {
                _content = options && options.content || "";
            }

            return _content ? require(_content) : "";
        }
    }
});