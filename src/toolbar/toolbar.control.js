/**
 * @class Toolbar
 * @classdesc Панель для кнопок
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.toolbar", [
    "control"
], function (Control) {

    return function (options) {

        return Control.extend({
            template: "#kv-toolbar-control",
            components: options && options.components || {},
            data: function () {
                return {};
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});
