/**
 * @class Carousel
 * @classdesc Компонент карусель
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.carousel", [
    "control",
    "controls.button",
    "controls.slide"
], function (Control, Button, Slide) {

    return function () {

        /**
         * Показать конкретный слайд
         * @param slide
         */
        Control.prototype.showSlide = function (slide) {
            if (slide) {
                // Скрывает все табы и показываем нужный
                this.jQuery(".kv-carousel-control__slide", this.getCssId()).css("display", "none");
                this.jQuery("[name='" + slide.getName() + "']", this.getCssId()).css("display", "block");
                // Подсвечиваем нижнюю кнопку
                this.jQuery(".bottom-btn", this.getCssId()).removeClass("bottom_selected");
                this.set("_current_index", slide.get("_slide_index"));
            }
        };

        /**
         * Запуск слайдов
         * @param _slides
         * @param _buttons
         * @private
         */
        Control.prototype._play = function (_slides, _buttons) {

            var _self = this,
                _$button;

            return setInterval(function () {

                _self.jQuery(".bottom-btn", _self.getCssId()).removeClass("kv-active");

                if (_self.get("_current_index") + 1 <= _self.get("_slide_count") - 1) {
                    _$button = _self.jQuery(_buttons[_self.get("_current_index") + 1].getCssId());
                    _self.showSlide(_slides[_self.get("_current_index") + 1]);
                } else {
                    _self.set("_current_index", 0);
                    _self.showSlide(_slides[0]);
                    _$button = _self.jQuery(_buttons[0].getCssId());
                }

                _$button.addClass("kv-active");

            }, _self.get("delay"));
        };

        return Control.extend({
            template: "#kv-carousel-control",
            components: {
                btnLeft: Button(),
                btnRight: Button(),
                btnCarousel: Button(),
                slide: Slide()
            },
            data: function () {
                return {
                    _buttons: [],
                    play: false,
                    delay: 3000,
                    _current_index: 0,
                    _slide_count: 0
                }
            },
            oninit: function () {
                var _self = this;

                _self._super();
            },
            onrender: function () {

                var _self = this,
                    $leftBtn = _self.jQuery(".left-btn", _self.getCssId()),
                    $rightBtn = _self.jQuery(".right-btn", _self.getCssId()),
                    _buttons = [],
                    _delay = _self.get("delay"),
                    _slides = _self.findAllComponents("slide");

                _slides.forEach(function (slide, _idx) {
                    slide.set("_slide_index", _idx);
                    _buttons.push(slide.getName());
                });

                _self.set("_buttons", _buttons);
                _self.set("_slide_count", _buttons.length);

                var _buttons = _self.findAllComponents("btnCarousel");

                _slides.forEach(function (slide, _idx) {

                    slide.set("_slide_index", _idx);

                    _buttons[_idx].subscribe("clickBtn", function () {
                        _self.showSlide(slide);
                        _self.jQuery(".bottom-btn", _self.getCssId()).removeClass("kv-active");
                        this.getDomInstance().addClass("kv-active");
                    });
                });

                _self.findComponent("btnLeft").subscribe("clickBtn", function (event) {

                    if (_self.get("_current_index") - 1 >= 0) {
                        //TODO подумать как передать original
                        _buttons[_self.get("_current_index") - 1].fire("clickBtn", event);
                    }
                });


                _self.findComponent("btnRight").subscribe("clickBtn", function (event) {

                    if (_self.get("_current_index") + 1 <= _self.get("_slide_count") - 1) {
                        //TODO подумать как передать original
                        _buttons[_self.get("_current_index") + 1].fire("clickBtn", event)
                    }
                });


                if (_delay < 1000) {
                    _delay = 1000;
                }

                // Устанавливаем свойства
                _self.set("delay", _delay || 3000);
                _self.set("play", _self.get("play") || false);

                // Устанавливаем слайд по умолчанию
                _self.showSlide(_slides[0]);
                _self.jQuery(_buttons[0].getCssId()).addClass("kv-active");

                // Определяем видимость кнопок от текущего слайда
                _self.observe("_current_index", function (newVal) {

                    if (newVal === 0) {
                        $leftBtn.css("visibility", "hidden")
                    } else {
                        $leftBtn.css("visibility", "visible");
                    }

                    if (newVal === _self.get("_slide_count") - 1) {
                        $rightBtn.css("visibility", "hidden")
                    } else {
                        $rightBtn.css("visibility", "visible");
                    }
                });

                var intervalId = null;

                _self.observe("play", function (newVal, oldVal) {

                    if (newVal === true && newVal !== oldVal) {
                        intervalId = _self._play(_slides, _buttons);
                    } else {
                        clearInterval(intervalId);
                    }
                });

                // Запускаем автопрокрутку
                if (_self.get("play") === true) {

                    _self.jQuery(".kv-carousel-control__content", _self.getCssId()).mouseover(function () {
                        _self.set("play", false)
                    });

                    _self.jQuery(".kv-carousel-control__content", _self.getCssId()).mouseout(function () {
                        _self.set("play", true);
                    })
                }

                _self._super();
            }
        });
    }
});