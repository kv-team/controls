<a name="Accordion"></a>

## Accordion ⇐ <code>Control</code>
Компонент список

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Data  
**Extend**: Filter  
**Extend**: Http  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| dataSource | <code>Array</code> | Перечень статей |
| name | <code>String</code> | Наименование для разработчика |
| parent | <code>Integer</code> | Ссылка на родительский элемент |

<a name="Control+closeAllItems"></a>

### accordion.closeAllItems()
Закрывает все пункты, кроме переданных как параметр

**Kind**: instance method of <code>[Accordion](#Accordion)</code>  

## Classes

<dl>
<dt><a href="#Breadcrumbs">Breadcrumbs</a> ⇐ <code>Control</code></dt>
<dd><p>Компонент список вложений</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#addLink">addLink(object)</a></dt>
<dd><p>Добавление узла</p>
</dd>
<dt><a href="#delLink">delLink(id)</a></dt>
<dd><p>Удаление узла по идентификатору</p>
</dd>
</dl>

<a name="Breadcrumbs"></a>

## Breadcrumbs ⇐ <code>Control</code>
Компонент список вложений

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Utils  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| items | <code>Array:json</code> | Источник данных |

<a name="addLink"></a>

## addLink(object)
Добавление узла

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| object | <code>json</code> | формат {id: 1, name: 'test'} |

<a name="delLink"></a>

## delLink(id)
Удаление узла по идентификатору

**Kind**: global function  

| Param | Type |
| --- | --- |
| id | <code>Integer</code> | 


<a name="Browser"></a>

## Browser ⇐ <code>Control</code>
Компонент браузер
Данный компонент является обязательным.
Первым делом при разработке страниц его необходимо добавить на форму.
Он необходим для консалидировании контекса страницы.

Требования при разработке:
1. Браузер должен быть один
2. Все элементы, которые будут содержать значения должны содержать аттрибут name заполненным

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Utils  
**Author:** A Ivanov  

* [Browser](#Browser) ⇐ <code>Control</code>
    * [._initContext()](#Control+_initContext)
    * [.getContext()](#Control+getContext) ⇒ <code>object</code>
    * [._setContext(component, name, key, val)](#Control+_setContext)
    * [.UI(selector)](#Control+UI) ⇒ <code>Object</code>
    * [.setUI(component)](#Control+setUI)
    * [.setLocale(locale)](#Control+setLocale)
    * [.t(key)](#Control+t)
    * [.initLocales(clientLocales)](#Control+initLocales)
    * [.getLocales()](#Control+getLocales)

<a name="Control+_initContext"></a>

### browser._initContext()
Инициализация компонентов из localStorage

**Kind**: instance method of <code>[Browser](#Browser)</code>  
**Throw**: Найдено несколько компонентов с одинаковым названием  
<a name="Control+getContext"></a>

### browser.getContext() ⇒ <code>object</code>
Метод получения всего контекста страницы

**Kind**: instance method of <code>[Browser](#Browser)</code>  
**Returns**: <code>object</code> - Выдает контекс компонента  
<a name="Control+_setContext"></a>

### browser._setContext(component, name, key, val)
Метод изменения параметров контекста

**Kind**: instance method of <code>[Browser](#Browser)</code>  
**Throw**: Не указано название browser  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>Object</code> | Наименование browser |
| name | <code>String</code> | Имя компонента |
| key | <code>String</code> | Название типа (id || value) |
| val | <code>Object</code> | Значение параметра |

<a name="Control+UI"></a>

### browser.UI(selector) ⇒ <code>Object</code>
Получение компонента по его наименованию

**Kind**: instance method of <code>[Browser](#Browser)</code>  
**Returns**: <code>Object</code> - Возвращает найденный компонент  
**Throw**: Не указан selector  

| Param | Type | Description |
| --- | --- | --- |
| selector | <code>String</code> | Имя компонента |

<a name="Control+setUI"></a>

### browser.setUI(component)
Инициализируем UI в browser

**Kind**: instance method of <code>[Browser](#Browser)</code>  

| Param | Type | Description |
| --- | --- | --- |
| component | <code>this</code> | экземпляр компонента |

<a name="Control+setLocale"></a>

### browser.setLocale(locale)
Устанавливаем локаль

**Kind**: instance method of <code>[Browser](#Browser)</code>  

| Param | Type |
| --- | --- |
| locale | <code>String</code> | 

<a name="Control+t"></a>

### browser.t(key)
Получение локали по ключу

**Kind**: instance method of <code>[Browser](#Browser)</code>  

| Param | Type |
| --- | --- |
| key | <code>String</code> | 

<a name="Control+initLocales"></a>

### browser.initLocales(clientLocales)
Инициализация локалей

**Kind**: instance method of <code>[Browser](#Browser)</code>  

| Param | Type | Description |
| --- | --- | --- |
| clientLocales | <code>Object</code> | {JSON} Локали загруженные из файла locales.json |

<a name="Control+getLocales"></a>

### browser.getLocales()
Получение списка локалей в виде JSON объекта

**Kind**: instance method of <code>[Browser](#Browser)</code>  

## Classes

<dl>
<dt><a href="#Button">Button</a> ⇐ <code>Control</code></dt>
<dd><p>компонент Кнопка</p>
</dd>
</dl>

## Events

<dl>
<dt><a href="#event_clickBtn">"clickBtn"</a></dt>
<dd><p>Нажатие на кнопку</p>
</dd>
</dl>

<a name="Button"></a>

## Button ⇐ <code>Control</code>
компонент Кнопка

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| value | <code>String:&#x27;Default&#x27;</code> | Заголовок на кнопке |
| icon | <code>String</code> | Название иконки бутстрапа |
| placeholder | <code>String</code> | Подсказка внутри поля |

<a name="event_clickBtn"></a>

## "clickBtn"
Нажатие на кнопку

**Kind**: event emitted  

<a name="Carousel"></a>

## Carousel ⇐ <code>Control</code>
Компонент карусель

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
<a name="Control+showSlide"></a>

### carousel.showSlide(slide)
Показать конкретный слайд

**Kind**: instance method of <code>[Carousel](#Carousel)</code>  

| Param |
| --- |
| slide | 


<a name="Carousel slide"></a>

## Carousel slide ⇐ <code>Control</code>
Компонент слайд

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  

## Classes

<dl>
<dt><a href="#CheckBox">CheckBox</a> ⇐ <code>Control</code></dt>
<dd><p>Компонент панель с выбором</p>
</dd>
</dl>

## Events

<dl>
<dt><a href="#event_clickCheck">"clickCheck"</a></dt>
<dd><p>Нажатие на компонент</p>
</dd>
</dl>

<a name="CheckBox"></a>

## CheckBox ⇐ <code>Control</code>
Компонент панель с выбором

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Resulted  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| caption | <code>String</code> | Текст в компоненте |

<a name="event_clickCheck"></a>

## "clickCheck"
Нажатие на компонент

**Kind**: event emitted  

<a name="DataGrid"></a>

## DataGrid ⇐ <code>Control</code>
Компонент таблица

**Kind**: global class  
**Extends:** <code>Control</code>, <code>Selected</code>, <code>Filter</code>, <code>Sorted</code>, <code>Data</code>, <code>Http</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| url | <code>String</code> | Путь по которому будет производиться запрос к серверу (POST) |
| columns | <code>Array</code> | Массив который хранит в себе конфигурацию колонок таблицы |

<a name="new_DataGrid_new"></a>

### new DataGrid()
Данный компонент предназначен для вывода данных с сервера в виде таблицы.
 Для хранения шаблонов ячеек реализована папка ./public/templates/cell
 У таблицы есть кнопка "Обновить" по умолчанию
 Так же в таблицу предустановлен набор кнопок экспорта данных (смотрите ExportBtns) и
 общих кнопок действий над записью (смотрите SharedBtns).


## Classes

<dl>
<dt><a href="#DatePicker">DatePicker</a> ⇐ <code>Control</code></dt>
<dd><p>Компонент календарь</p>
</dd>
</dl>

## Events

<dl>
<dt><a href="#event_keyDown">"keyDown"</a></dt>
<dd><p>Нажатие на кнопку Escape</p>
</dd>
<dt><a href="#event_clickBtn">"clickBtn"</a></dt>
<dd><p>Нажатие на кнопку раскрытия списка</p>
</dd>
</dl>

<a name="DatePicker"></a>

## DatePicker ⇐ <code>Control</code>
Компонент календарь

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Resulted  
**Extend**: OpenExtend  
**Author:** A Ivanov  
<a name="event_keyDown"></a>

## "keyDown"
Нажатие на кнопку Escape

**Kind**: event emitted  
<a name="event_clickBtn"></a>

## "clickBtn"
Нажатие на кнопку раскрытия списка

**Kind**: event emitted  

## Classes

<dl>
<dt><a href="#Dialog">Dialog</a> ⇐ <code>Control</code></dt>
<dd><p>компонент Диалог/Модальный диалог</p>
</dd>
</dl>

## Events

<dl>
<dt><a href="#event_clickBtnEnter">"clickBtnEnter"</a></dt>
<dd><p>Событие нажатия на кнопку отправки</p>
</dd>
<dt><a href="#event_clickBtnCancel">"clickBtnCancel"</a></dt>
<dd><p>Событие нажатия на кнопку отмена</p>
</dd>
</dl>

<a name="Dialog"></a>

## Dialog ⇐ <code>Control</code>
компонент Диалог/Модальный диалог

**Kind**: global class  
**Extends:** <code>Control</code>, <code>Http</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| formTemplate | <code>String</code> | Шаблон контента диалога.           Сам шаблон должен храниться в папке ./public/templates/form |
| modal | <code>boolean</code> &#124; <code>false</code> | Признак типа диалога. Если true то модальный |
| caption | <code>String:&#x27;Default title&#x27;</code> | Заголовок диалога |
| btnOk | <code>String:&#x27;Ok&#x27;</code> | Заголовок для кнопки отправить |
| _controlsResult | <code>String</code> | Компоненты которые будут анализироваться в диалоге |
| _alerts | <code>String</code> | Перечень классов CSS которые будут нужны при проверке |


* [Dialog](#Dialog) ⇐ <code>Control</code>
    * [.hide()](#Control+hide)
    * [.show()](#Control+show)
    * [.isOk()](#Control+isOk) ⇒ <code>boolean</code>
    * [.getRequestParams()](#Control+getRequestParams) ⇒ <code>Object</code>

<a name="Control+hide"></a>

### dialog.hide()
Скрыть диалог

**Kind**: instance method of <code>[Dialog](#Dialog)</code>  
<a name="Control+show"></a>

### dialog.show()
Показать диалог

**Kind**: instance method of <code>[Dialog](#Dialog)</code>  
<a name="Control+isOk"></a>

### dialog.isOk() ⇒ <code>boolean</code>
Проверка на возможность отправить значения

**Kind**: instance method of <code>[Dialog](#Dialog)</code>  
**Returns**: <code>boolean</code> - Признак можно ли отправить данные на сервер  
<a name="Control+getRequestParams"></a>

### dialog.getRequestParams() ⇒ <code>Object</code>
Получение значений формы

**Kind**: instance method of <code>[Dialog](#Dialog)</code>  
<a name="event_clickBtnEnter"></a>

## "clickBtnEnter"
Событие нажатия на кнопку отправки

**Kind**: event emitted  
<a name="event_clickBtnCancel"></a>

## "clickBtnCancel"
Событие нажатия на кнопку отмена

**Kind**: event emitted  

<a name="Exports Buttons"></a>

## Exports Buttons ⇐ <code>Control</code>
Компонент группа кнопок экспорта

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| isExcel | <code>boolean</code> | вывести кнопку экспорта в Excel |
| isWord | <code>boolean</code> | вывести кнопку экспорта в Word |
| isPdf | <code>boolean</code> | вывести кнопку экспорта в Pdf |


<a name="FieldBtnControl"></a>

## FieldBtnControl ⇐ <code>Control</code>
Панель ввода с кнопкой

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: OpenExtend  
**Author:** A Ivanov  

<a name="FieldControl"></a>

## FieldControl ⇐ <code>Control</code>
Компонент для ввода значения

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Resulted  
**Extend**: Validate  
**Extend**: Mask  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| placeholder | <code>String</code> | Подсказка для пользователя |
| validate | <code>String</code> | Метод валидации из Validate |
| mask | <code>String</code> | Метод маскирования из Mask |
| text | <code>String</code> | Имя поля из БД (обязательно для заполнения) |


<a name="Frame"></a>

## Frame ⇐ <code>Control</code>
Компонент рамка/подложка

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| name | <code>String</code> | Название для разработчика |


<a name="Image"></a>

## Image ⇐ <code>Control</code>
Компонент изображение

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| name | <code>String</code> | Наименование для разработчика |
| url | <code>String</code> | Ссылка для запроса с сервера |
| _alt | <code>String</code> | Текст подсказки при отсутствии изображения |


<a name="Label"></a>

## Label ⇐ <code>Control</code>
Компонент текст

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| value | <code>String:&#x27;Default header&#x27;</code> | Текст заголовка |
| icon | <code>String:&#x27;&#x27;</code> | Иконка |


<a name="langControl"></a>

## langControl ⇐ <code>Control</code>
Компонент с языковой панелью

**Kind**: global class  
**Extends:** <code>Control</code>  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| _languages | <code>array</code> | перечень языков |


<a name="LinkControl"></a>

## LinkControl ⇐ <code>Control</code>
Компонент ссылка

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| value | <code>String</code> | Текст в ссылке |
| icon | <code>String</code> | Иконка из bootstrap |
| url | <code>String</code> | Ссылка по которой переходим по ссылке |


<a name="LoadPanel"></a>

## LoadPanel ⇐ <code>Control</code>
Компонент загрузка

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  

## Classes

<dl>
<dt><a href="#Lookup">Lookup</a> ⇐ <code>Control</code></dt>
<dd><p>Выпадающий список</p>
</dd>
</dl>

## Events

<dl>
<dt><a href="#event_keyDown">"keyDown"</a></dt>
<dd><p>Нажатие на кнопку Escape</p>
</dd>
<dt><a href="#event_clickBtn">"clickBtn"</a></dt>
<dd><p>Нажатие на кнопку раскрытия списка</p>
</dd>
<dt><a href="#event_clickItem">"clickItem"</a></dt>
<dd><p>Нажатие на строку списка</p>
</dd>
</dl>

<a name="Lookup"></a>

## Lookup ⇐ <code>Control</code>
Выпадающий список

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Data  
**Extend**: Resulted  
**Extend**: Filter  
**Extend**: Http  
**Extend**: OpenExtend  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| items | <code>Array:Object</code> | Коллекция данных |
| text | <code>String:&#x27;name&#x27;</code> | Колонка из БД которая отображает наименование |
| url | <code>String</code> | Ссылка для запроса данных с с сервера |
| placeholder | <code>String</code> | Подсказка внутри поля для ввода |
| _isOpen | <code>boolean</code> | Признак открытого списка |

<a name="event_keyDown"></a>

## "keyDown"
Нажатие на кнопку Escape

**Kind**: event emitted  
<a name="event_clickBtn"></a>

## "clickBtn"
Нажатие на кнопку раскрытия списка

**Kind**: event emitted  
<a name="event_clickItem"></a>

## "clickItem"
Нажатие на строку списка

**Kind**: event emitted  

<a name="Memo"></a>

## Memo ⇐ <code>Control</code>
Поле ввода memo

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Resulted  
**Extend**: Validate  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| placeholder | <code>String</code> | Подсказка в поле для ввода |
| validate | <code>String</code> | Метод проверки значения |


<a name="Menu"></a>

## Menu ⇐ <code>Control</code>
Компонент меню

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: OpenExtend  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| logoUrl | <code>String</code> | Путь к логотипу |
| items | <code>Array</code> | Список пунктов меню слева |
| items_right | <code>Array</code> | Список пунктов меню справа |


* [Menu](#Menu) ⇐ <code>Control</code>
    * [.activate($item)](#Control+activate)
    * [.getZipItems()](#Control+getZipItems) ⇒ <code>Array</code>
    * [.showSubMenu($item)](#Control+showSubMenu)

<a name="Control+activate"></a>

### menu.activate($item)
Активация пунктов меню

**Kind**: instance method of <code>[Menu](#Menu)</code>  

| Param | Type | Description |
| --- | --- | --- |
| $item | <code>Object</code> | Пункт меню |

<a name="Control+getZipItems"></a>

### menu.getZipItems() ⇒ <code>Array</code>
Получение списка свернутых пунктов меню

**Kind**: instance method of <code>[Menu](#Menu)</code>  
<a name="Control+showSubMenu"></a>

### menu.showSubMenu($item)
Отображение блока подменю

**Kind**: instance method of <code>[Menu](#Menu)</code>  

| Param | Type | Description |
| --- | --- | --- |
| $item | <code>LinkControl</code> | компонент ссылка |


<a name="Panel"></a>

## Panel ⇐ <code>Control</code>
Копмонент панель

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  

* [Panel](#Panel) ⇐ <code>Control</code>
    * [.show()](#Control+show)
    * [.hide()](#Control+hide)

<a name="Control+show"></a>

### panel.show()
Открытие панели

**Kind**: instance method of <code>[Panel](#Panel)</code>  
<a name="Control+hide"></a>

### panel.hide()
Закрытие панели

**Kind**: instance method of <code>[Panel](#Panel)</code>  

## Classes

<dl>
<dt><a href="#Radio">Radio</a> ⇐ <code>Control</code></dt>
<dd><p>Компонент выборка значения</p>
</dd>
</dl>

## Events

<dl>
<dt><a href="#event_onClick">"onClick"</a></dt>
<dd><p>Нажатие на позицию списка</p>
</dd>
</dl>

<a name="Radio"></a>

## Radio ⇐ <code>Control</code>
Компонент выборка значения

**Kind**: global class  
**Extends:** <code>Control</code>  
**Extend**: Resulted  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| _dataSource | <code>Array:json</code> | Перечень значений для компонента {id: 111, name: 'dsadad'} |

<a name="event_onClick"></a>

## "onClick"
Нажатие на позицию списка

**Kind**: event emitted  

## Classes

<dl>
<dt><a href="#ScrollBtn">ScrollBtn</a> ⇐ <code>Control</code></dt>
<dd><p>Кнопка скролла страницы</p>
</dd>
</dl>

## Members

<dl>
<dt><a href="#delay">delay</a></dt>
<dd><p>В каком положении полосы прокрутки начинать показ кнопки &quot;Наверх&quot;</p>
</dd>
</dl>

<a name="ScrollBtn"></a>

## ScrollBtn ⇐ <code>Control</code>
Кнопка скролла страницы

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
<a name="delay"></a>

## delay
В каком положении полосы прокрутки начинать показ кнопки "Наверх"

**Kind**: global variable  

<a name="SharedButtons"></a>

## SharedButtons ⇐ <code>Control</code>
Кнопки с основными действиями над записями

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  

<a name="SocialButtons"></a>

## SocialButtons ⇐ <code>Control</code>
Компонент кнопки социальных сетей

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| name | <code>String</code> | Название для разработчика |
| isYt | <code>boolean</code> | отображение кнопки Ютуба |
| isFB | <code>boolean</code> | отображение кнопки Фейсбука |
| isVK | <code>boolean</code> | отображение кнопки Вконтакте |
| isTW | <code>boolean</code> | отображение кнопки Твиттер |
| isOD | <code>boolean</code> | отображение кнопки Одноклассники |


<a name="Tab"></a>

## Tab ⇐ <code>Control</code>
Закладка

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| content | <code>String</code> | Шаблон закладки. Должен храниться в папке ./public/templates/tabs. |

<a name="Control+getCaption"></a>

### tab.getCaption() ⇒ <code>string</code>
Получение названия вкладки

**Kind**: instance method of <code>[Tab](#Tab)</code>  

<a name="Tabs"></a>

## Tabs ⇐ <code>Control</code>
Компонент панель для вкладок

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| items | <code>Array:String</code> | Массив ссылок генерируется автоматически |
| tabDefault | <code>String</code> | Вкладка по умолчанию |

<a name="Control+showTab"></a>

### tabs.showTab(tabName)
Открытие конкретной вкладки по name

**Kind**: instance method of <code>[Tabs](#Tabs)</code>  

| Param |
| --- |
| tabName | 


<a name="Toolbar"></a>

## Toolbar ⇐ <code>Control</code>
Панель для кнопок

**Kind**: global class  
**Extends:** <code>Control</code>  
**Author:** A Ivanov  
