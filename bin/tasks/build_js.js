/**
 * Данные задачи отвечают за модулей js для компонентов
 *
 * @author Иванов Александр Сергеевич
 * @date 21 марта 2017
 */
var fs = require("fs"),
    path = require("path"),
    gulp = require("gulp"),
    gulpsync = require("gulp-sync")(gulp),
    clean = require("gulp-clean"),
    stripJsComments = require("gulp-strip-comments"),
    concat = require("gulp-concat"),
    minify = require("gulp-minify"),
    watch = require("gulp-watch"),
    browserSync = require("browser-sync"),
    include = require("gulp-file-include"),
    testMocha = require("gulp-mocha"),
    jsonMinify = require("gulp-jsonminify"),
    errors = require("../errors"),
    config = require("../../config");

/**
 * Сборка js модулей для web компонентов
 */
gulp.task("build:components:js", gulpsync.sync([
    "clean:components:js",
    "concat:components:js",
    "compile:requirejs",
    "compile:components:json"
]), function () {
});

/**
 * Удаление модулей js у компонентов
 */
gulp.task("clean:components:js", function () {

    if (fs.existsSync(config.dirBuild())) {

        config.taskStart("clean:components:js");

        return gulp.src([
            path.join(config.dirBuild(), "**/*.js"),
            path.join(config.dirBuild(), "*.js")
        ])
            .pipe(clean({force: true})
                .on("data", function () {
                    config.blockOk("Clear file")
                })
                .on("error", errors.ClearError))
            .on("finish", function () {
                config.taskEnd("clean:components:js");
            });
    } else {
        fs.mkdirSync(config.dirBuild())
    }

});

/**
 * Сборка всех js компонентов в один. Удаляем все комментарии в коде.
 */
gulp.task("concat:components:js", function () {

    config.taskStart("concat:components:js");

    return gulp.src(path.join(config.dirSrc(), "**/*.js"))
        .pipe(include({
            prefix: "@@",
            basePath: "../src/"
        }))
        .pipe(stripJsComments()
            .on("data", function () {
                config.blockOk("Strip comment JS")
            })
            .on("error", errors.CommentJsError))
        .pipe(minify({
            ext: {
                min: ".min.js"
            }
        })
            .on("data", function () {
                config.blockOk("Minimized JS")
            })
            .on("error", errors.MinJsError))
        .pipe(gulp.dest(config.dirBuild()))
        .on("finish", function () {
            config.taskEnd("concat:components:js")
        })
        .pipe(browserSync.reload({stream: true}));
});

/**
 * Слушаем изменения в файлах модулей
 */
gulp.task("watch:components:js", function () {

    config.watchStart("watch:components:js");

    watch(path.join(config.dirSrc(), "/**/*.js"), function () {
        gulp.start("build:components:js");
    });
});

/**
 * Тестирование компонентов
 */
gulp.task("UnitTest:components", function () {

    config.taskStart("test:components");

    return gulp.src(path.join(config.dirTest(), "*.js"))
        .pipe(testMocha())
});

/**
 * Локализация компонентов
 */
gulp.task("compile:components:json", function () {

    config.taskStart("compile:components:json");

    return gulp.src([
        path.join(config.dirSrc(), "locales.json"),
        path.join(config.dirSrc(), "room/3dRoomBuild.json"),
        path.join(config.dirSrc(), "room/3dRoomBuild.asm.code.unityweb"),
        path.join(config.dirSrc(), "room/3dRoomBuild.asm.framework.unityweb"),
        path.join(config.dirSrc(), "room/3dRoomBuild.asm.memory.unityweb"),
        path.join(config.dirSrc(), "room/3dRoomBuild.asm.data.unityweb")
    ])
        .pipe(jsonMinify())
        .pipe(gulp.dest(config.dirBuild()))
        .on("finish", function () {
            config.taskEnd("compile:components:json");
        });
});

