/**
 * @class http
 * @classdesc Расширение для отправки сообщений через AJAX.
 *
 * @author A Ivanov
 */
define("mixin.http", [], function () {

    return {
        data: function () {
            return {
                url: ""
            };
        },
        /**
         * Отправка запроса ajax GET
         * @param data {json} данные для отправки на сервер, преобразовывать в строку не нужно
         * @param callback результат запроса
         * @throws Не указана ссылка для отправки GET запроса
         */
        $get: function (data, callback) {

            if (!this._.isUndefined(this.get("url")) && this.get("url") > "") {
                this.jQuery.get({
                    url: this.get("url"),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(data)
                }).then(callback);
            } else {
                console.warn("Не указана ссылка для отправки GET запроса");
            }
        },
        /**
         * Отправка запроса ajax POST
         * @param data {json} данные для отправки на сервер, преобразовывать в строку не нужно
         * @param callback результат запроса
         * @throws Не указана ссылка для отправки POST запроса
         */
        $post: function (data, callback) {

            if (!this._.isUndefined(this.get("url")) && this.get("url") > "") {
                this.jQuery.post({
                    url: this.get("url"),
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(data)
                }).then(callback);
            } else {
                console.warn("Не указана ссылка для отправки POST запроса");
            }
        },
        oninit: function () {
            var _self = this;
            _self._super();
        }
    }
});