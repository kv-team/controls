/**
 * @class Button
 * @classdesc компонент Кнопка
 * @extends Control
 *
 * @property value {String:"Default"} Заголовок на кнопке
 * @property icon {String} Название иконки бутстрапа
 * @property placeholder {String} Подсказка внутри поля
 *
 * @author A Ivanov
 */
define("controls.button", [
    "control"
], function (Control) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-btn-control",
            data: function () {
                return {
                    value: options && options.value || "Default",
                    icon: options && options.icon || "",
                    placeholder: options && options.placeholder || ""
                }
            },
            oninit: function () {
                var _self = this;

                /**
                 * Нажатие на кнопку
                 * @event clickBtn
                 */
                _self.subscribe("clickBtn", function (event) {
                    event.original.preventDefault();
                });

                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});