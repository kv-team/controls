var yml = require('yamljs');
var expect = require('chai').expect;
var configComponents = yml.load('config/properties.yml');

var TestComponent = function (componentName, instancePage) {
    var _self = this;

    _self._componentName = componentName;
    _self._configComponent = configComponents['controls'][_self._componentName];

    if (!_self._configComponent) {
        throw Error('Отсутвует конфигурация для компонента: ' + _self._componentName);
    }

    _self._components = instancePage.getInstance().findAllComponents(_self._componentName);

    describe('Тестирование компонента: ' + _self._componentName, function () {
        _self.testProperties();
        _self.testMethods();
    });
};

TestComponent.prototype.testProperties = function () {
    var _self = this;

    describe('Тестирование свойств компонента', function () {
        for (var _property in _self._configComponent['properties']) _self.testProperty(_property);
    });
};

TestComponent.prototype.testMethods = function () {
    var _self = this;

    describe('Тестирование методов компонента', function () {

        for (var _method in _self._configComponent['methods']) _self.testMethod(_method);
    });
};

TestComponent.prototype.testProperty = function (_property) {
    var _self = this,
        _config = _self._configComponent['properties'][_property];

    describe('Property: (' +  _property + ') ' + (_config['desc'] || 'Не определено'), function () {

        _self._components.forEach(function (_component) {

            describe('Instance: ' + (_component.getName() || 'Не указано наименование!'), function () {

                it('Наличие свойства', function (done) {
                    expect(_component.viewmodel.value[_property]).not.to.be.a('undefined');
                    done();
                });

                /**
                 * Проверка типа свойства
                 */
                if (_config['type']) {

                    it('Соответствие типу', function (done) {
                        expect(_component.viewmodel.value[_property]).to.be.a(_config['type']);
                        done();
                    });
                }
            });
        });
    });
};

TestComponent.prototype.testMethod = function (_method) {
    var _self = this,
        _config = _self._configComponent['methods'][_method];

    describe('Method: (' + _method + ') ' + (_config['desc'] || 'Не определен'), function () {

        _self._components.forEach(function (_component) {

            describe('Instance: ' + (_component.getName() || 'Не указано наименование!'), function () {

                it('Наличие метода', function (done) {
                    expect(_component[_method]).not.to.be.a('undefined');
                    done();
                });

                if (_config['return']) {

                    it('Соответствие выходного значения типу', function (done) {

                        if (_config['return'] && !_config['return_throw']) {
                            expect(_component[_method]()).to.be.a(_config['return']);
                        } else {
                            expect(_component[_method]).to.throw(_config['return_throw']);
                        }

                        done();
                    });
                }
            });
        });
    });
};

module.exports = TestComponent;