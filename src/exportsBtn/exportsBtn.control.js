/**
 * @class Exports Buttons
 * @classdesc Компонент группа кнопок экспорта
 * @extends Control
 *
 * @property isExcel {boolean} вывести кнопку экспорта в Excel
 * @property isWord {boolean} вывести кнопку экспорта в Word
 * @property isPdf {boolean} вывести кнопку экспорта в Pdf
 *
 * @author A Ivanov
 */
define("controls.exportsBtn", [
    "control",
    "controls.button"
], function (Control, Button) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-exports-btns-control",
            components: {
                button: Button()
            },
            data: function () {
                return {
                    isExcel: options && options.isExcel || false,
                    isWord: options && options.isWord || false,
                    isPdf: options && options.isPdf || false
                };
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});