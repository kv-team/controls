/**
 * Данные задачи отвечают за сборку шаблонов для компонентов
 *
 * @author Иванов Александр Сергеевич
 * @date 21 марта 2017
 */

var fs = require("fs"),
    path = require("path"),
    gulp = require("gulp"),
    gulpsync = require("gulp-sync")(gulp),
    clean = require("gulp-clean"),
    watch = require("gulp-watch"),
    browserSync = require("browser-sync"),
    pugConverter = require("gulp-pug"),
    concat = require("gulp-concat"),
    minify = require("gulp-html-minifier"),
    replace = require("gulp-replace"),
    errors = require("../errors"),
    config = require("../../config");

/**
 * Сборка html для web компонентов
 */
gulp.task("build:components:html", gulpsync.sync([
    "clean:components:html",
    "compile:components:pug"
]), function () {
});

/**
 * Удаление шаблонов у компонентов
 */
gulp.task("clean:components:html", function () {

    if (fs.existsSync(config.dirBuild())) {

        config.taskStart("clean:components:html");

        return gulp.src(path.join(config.dirBuild(), "**/*.html"))
            .pipe(clean({force: true})
                .on("data", function () {
                    config.blockOk("Clear file")
                })
                .on("error", errors.ClearError))
            .on("finish", function () {
                config.taskEnd("clean:components:html");
            });
    }
});

/**
 * Компиляция html из pug файлов
 */
gulp.task("compile:components:pug", function () {

    return gulp.src(path.join(config.dirSrc(), "**/*.pug"))
        .pipe(concat("components.pug"))
        .pipe(gulp.dest(config.dirBuild()))
        .pipe(pugConverter())
        .on("data", function () {
            config.blockOk("Compile PUG")
        })
        .on("error", errors.CompilePugToHtml)
        .pipe(concat("components.html"))
        .pipe(gulp.dest(config.dirBuild()))
        .on("finish", function () {
            config.taskEnd("compile:components:pug")
        })
        .pipe(browserSync.reload({stream: true}));

    config.taskStart("compile:components:pug");
});

/**
 * Слушаем изменения в файлах шаблонов
 */
gulp.task("watch:components:pug", function () {

    config.watchStart("watch:components:pug");

    watch(path.join(config.dirSrc(), "/**/*.pug"), function () {

        gulp.start("build:components:html");
    });
});