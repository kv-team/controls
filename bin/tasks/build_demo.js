/**
 * Данные задачи отвечают за сборку DEMO сайта
 *
 * @author Иванов Александр Сергеевич
 * @date 21 марта 2017
 */

var path = require("path"),
    gulp = require("gulp"),
    gulpsync = require("gulp-sync")(gulp),
    config = require("../../config"),
    less = require("gulp-less"),
    autoprefixer = require("gulp-autoprefixer"),
    stripCssComments = require("gulp-strip-css-comments"),
    pugConverter = require("gulp-pug"),
    browserSync = require("browser-sync"),
    replace = require("gulp-replace"),
    errors = require("../errors"),
    watch = require("gulp-watch");

/**
 * Автоматическое сборка демо сайта
 */
gulp.task("watch:demo", [
    "watch:demo:pug",
    "watch:demo:js",
    "watch:demo:less"
], function () {
});

/**
 * Сборка демо сайта
 */
gulp.task("build:demo", gulpsync.sync(["compile:demo:less", "compile:demo:pug"]), function () {

});

/**
 * Компилирование стилевых описаний для демо сайта
 */
gulp.task("compile:demo:less", function () {

    config.taskStart("compile:demo:less");

    return gulp.src(path.join(config.dirDemo(), "*.less"))
        .pipe(less()
            .on("data", function () {
                config.blockOk("Compile LESS")
            })
            .on("error", errors.LessError))
        .pipe(autoprefixer({
            browsers: ["last 5 versions"],
            cascade: false
        })
            .on("data", function () {
                config.blockOk("Autoprefixer CSS")
            })
            .on("error", errors.PrefixError))
        .pipe(stripCssComments()
            .on("data", function () {
                config.blockOk("Strip comment CSS")
            })
            .on("error", errors.CommentCssError))
        .pipe(gulp.dest(config.dirDemo()))
        .on("finish", function () {
            config.taskEnd("compile:demo:less")
        })
        .pipe(browserSync.reload({stream: true}));

});

/**
 * Компилирование html из pug
 */
gulp.task("compile:demo:pug", function () {

    config.taskStart("compile:demo:pug");

    return gulp.src([
        path.join(config.dirDemo(), "*.pug"),
        path.join(config.dirDemo(), "pages/**/*.pug"),
        path.join(config.dirDemo(), "forms/*.pug"),
        path.join(config.dirDemo(), "tabs/*.pug"),
        path.join(config.dirDemo(), "slides/*.pug"),
        path.join(config.dirDemo(), "partials/*.pug")
    ])
        .pipe(replace("{{", "<!%"))
        .pipe(replace("}}", "%!>"))
        .pipe(pugConverter())
        .on("data", function () {
            config.blockOk("Compile PUG")
        })
        .on("error", errors.CompilePugToHtml)
        .pipe(replace("<!%", "{{"))
        .pipe(replace("%!>", "}}"))
        .pipe(replace("&lt;", "<"))
        .pipe(replace("&gt;", ">"))
        .pipe(replace("<!%", "{{"))
        .pipe(replace("%!>", "}}"))
        .pipe(gulp.dest(function (file) {
            return file.base;
        }))
        .on("finish", function () {
            config.taskEnd("compile:demo:pug")
        })
        .pipe(browserSync.reload({stream: true}));
});

/**
 * Слушатель изменений в стилях на демо сайте
 */
gulp.task("watch:demo:less", function () {

    config.watchStart("watch:demo:less");

    watch([path.join(config.dirDemo(), "*.less"), path.join(config.dirDemo(), "partials/*.less")], function () {

        gulp.start("compile:demo:less");
    });
});

/**
 * Слушатель изменений в шаблоне на демо сайте
 */
gulp.task("watch:demo:pug", function () {

    config.watchStart("watch:demo:pug");

    watch([
        path.join(config.dirDemo(), "*.pug"),
        path.join(config.dirDemo(), "pages/**/*.pug"),
        path.join(config.dirDemo(), "forms/*.pug"),
        path.join(config.dirDemo(), "tabs/*.pug"),
        path.join(config.dirDemo(), "slides/*.pug"),
        path.join(config.dirDemo(), "partials/*.pug")
    ], function () {

        gulp.start("compile:demo:pug");
    });
});

/**
 * Слушатель изменений в модулях js на демо сайте
 */
gulp.task("watch:demo:js", function () {

    config.watchStart("watch:demo:js");

    watch(path.join(config.dirDemo(), "*.js"), function () {

        gulp.src(path.join(config.dirDemo(), "*.js"))
            .pipe(browserSync.reload({stream: true}));
    });
});