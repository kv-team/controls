/**
 * @class Validate
 * @classdesc Расширение для применения валидации
 *
 * @property _empty Признак обязательного заполнения.
 *
 * @author A Ivanov
 */
define("mixin.validate", [], function () {

    return {
        // -------------------------------------------------------------------------------------------------
        // Public method
        // -------------------------------------------------------------------------------------------------
        /**
         * Проверка значения мобильный телефон
         * @param val {string} значение
         */
        isMobilePhone: function (val) {
            this._initAlert(val, /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/);
        },
        /**
         * Проверка значения почта
         * @param val {string} значение
         */
        isMail: function (val) {
            this._initAlert(val, /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
        },
        /**
         * Проверка на обязательность заполнения
         * @param val {string} значение
         */
        isEmpty: function (val) {
            if (val === null) {
                this._addWarning();
                return false;
            } else {
                this._deleteWarning();
                return true;
            }
        },
        // -------------------------------------------------------------------------------------------------
        // Private method
        // -------------------------------------------------------------------------------------------------
        /**
         * Инициализация стиля для control
         * @param val {string} значение
         * @param pattern {string} регулярное выражение
         */
        _initAlert: function (val, pattern) {
            if (val === null) {
                this._addError();
            } else {

                if (val !== undefined && val !== null) {
                    if (!pattern.test(val)) {
                        this._addError();
                    } else {
                        this._deleteError();
                    }
                } else {
                    this._deleteError();
                }
            }
        },
        /**
         * Устанавливаем стиль для ошибок
         */
        _addError: function () {

            var _$field = this.getDomInstance();

            if (!_$field.hasClass("kv-alert-error")) {
                _$field.addClass("kv-alert-error");
            }
        },
        /**
         * Устанавливаем стиль для предупреждений
         */
        _addWarning: function () {
            var _$field = this.getDomInstance();

            if (!_$field.hasClass("kv-alert-warning")) {
                _$field.addClass("kv-alert-warning");
            }
        },
        /**
         * Убираем стиль для ошибок
         */
        _deleteError: function () {
            this.getDomInstance().removeClass("kv-alert-error");
        },
        /**
         * Убираем стиль для предупреждений
         */
        _deleteWarning: function () {
            this.getDomInstance().removeClass("kv-alert-warning");
        },
        data: function () {
            return {
                // Признак обязательного заполнения
                _empty: true
            }
        },
        oninit: function () {
            // Устанавливаем флаг для отображения подсказки при обязательном вводе
            if (this.get("validate") && this.get("validate").indexOf("isEmpty") !== -1) {
                this.set("_empty", false);
            }

            this._super();
        },
        onrender: function () {
            var _self = this;
            _self._super();
        }
    }
});