/**
 * @class Breadcrumbs
 * @classdesc Компонент список вложений
 * @extends Control
 * @extend Utils
 *
 * @property items {Array:json} Источник данных
 *
 * @author A Ivanov
 */
define("controls.breadcrumbs", [
    "control",
    "mixin.utils",
    "controls.linkControl"
], function (Control, UtilsMixin, LinkControl) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-breadcrumbs-control",
            components: {
                linkControl: LinkControl()
            },
            data: function () {
                return {
                    items: options && options.items || []
                }
            },
            /**
             * Добавление узла
             * @param object {json} формат {id: 1, name: "test"}
             */
            addLink: function (object) {
                this.get("items").push(object);
            },
            /**
             * Удаление узла по идентификатору
             * @param id {Integer}
             */
            delLink: function (id) {
                this.set("items", this.removeObjToArrayByObjId(this.get("items"), id));
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        }, UtilsMixin);
    }
});