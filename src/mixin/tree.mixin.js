/**
 * @class Tree
 * @classdesc Расширение для работы с древовидной структурой
 *
 * @property parent {integer|null} Ссылка на родительский элемент
 *
 * @author A Ivanov
 */
define("mixin.tree", []
    , function () {

        return {
            data: function () {
                return {
                    parent: null, // ссылка на имя родительского элемента
                    _nodes: [],
                    _child: []
                }
            },
            /**
             * Инициализация всех узлов
             * return {Array} Перечень всех узлов
             */
            _setNodes: function () {

                var _results = [];

                this.get("dataSource").forEach(function(row) {
                    if (row.parent === null) {
                        _results.push(row);
                    }
                });

                this.set("_nodes", _results);
            },

            oninit: function () {
                var _self = this;

                _self._setNodes();

                _self._super();
            }
        }
    });
