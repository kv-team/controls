/**
 * Данные задачи отвечают за сборку конфигурации для require.js
 *
 * @author Иванов Александр Сергеевич
 * @date 21 марта 2017
 */

var fs = require("fs"),
    gulp = require("gulp"),
    yml = require("yamljs"),
    path = require("path"),
    config = require("../../config"),
    watch = require("gulp-watch");

/**
 * Компилируем конфигурационный файл requireJS
 */
gulp.task("compile:requirejs", function () {

    config.taskStart("compile:requirejs");


    yml.load("../config/require.config.yml", function (configPrepare) {

        try {
            if (configPrepare !== undefined && configPrepare !== null) {

                // Build
                var configBuild = configPrepare;
                configBuild.baseUrl = config.dirBuild();

                config.blockOk("Compile YML");

                fs.writeFile(path.join(config.dirBuild(), "require.main.js"), "require.config(" + JSON.stringify(configBuild) + ");", "utf-8", function (err) {
                    if (err) {

                        config.blockNo("Compile main");
                        config.taskEnd("compile:requirejs");
                    } else {
                        config.blockOk("Compile main");

                        // Test
                        var configTest = configPrepare;
                        configTest.baseUrl = path.join(__dirname, "../../build");

                        fs.writeFile(path.join(config.dirTest(), "require.test.json"), JSON.stringify(configTest), "utf-8", function (err) {
                            if (err) {

                                config.blockNo("Compile test");
                                config.taskEnd("compile:requirejs");
                            } else {
                                config.blockOk("Compile test");
                                config.taskEnd("compile:requirejs");
                            }
                        });
                    }
                });


            } else {
                config.blockNo("File is empty");
                config.taskEnd("compile:requirejs");
            }

        } catch (e) {
            config.blockNo("Compile YML");
            config.taskEnd("compile:requirejs");
            this.emit("end");
        }
    });


});

/**
 * Мониторим изменения в конфиге компонентов
 */
gulp.task("watch:requirejs", function () {

    config.watchStart("watch:requirejs");

    watch("../config/require.config.yml", function () {
        gulp.start("compile:requirejs");
    })
});
