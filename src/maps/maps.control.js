/**
 * @class maps
 * @extends Control
 */
define("controls.maps", [
    "control"
], function (Control) {
    return function (options) {
        
        Control.prototype.getMap = function () {
          return this.get("map") || null;
        };

        return Control.extend({
            isolated: true,
            template: "#kv-maps-control",
            data: function () {
                return {
                    center: options && options.center ? options.center : [60.106778, 30.268707],
                    map: null
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();

                var map;

                window.ymaps.ready(function () {
                    var map = new window.ymaps.Map(_self.getId(), {
                            center: _self.get("center"),
                            zoom: 10
                        }),
                        myPlacemark = new ymaps.Placemark(_self.get("center"), {}, {
                            preset: "twirl#redIcon"
                        });

                    map.geoObjects.add(myPlacemark);

                    map.controls.add(
                        new window.ymaps.control.ZoomControl()
                    );

                    _self.set("map", map);
                });


            }
        })
    }
});