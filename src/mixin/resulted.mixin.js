/**
 * @class Resulted
 * @classdesc Расширение для получения значения control.
 *
 *  @property _result {String} Значение
 *  @property _result_id {Integer} Идентификатор значения
 *
 * @author A Ivanov
 */
define("mixin.resulted", [
    "mixin.utils"
], function (UtilsMixin) {

    return {
        data: function () {
            return {
                _result: null,
                _result_id: null
            }
        },
        /**
         * Получение значения control
         * @return {String|null}
         */
        getResult: function () {
            var _result;

            if (this.getComponentName() === 'lookup') {
                _result = this.findComponent('fieldText').getResult() || null;
            } else {
                _result = this.get("_result_id") || String(this.get("_result")).trim() || null;
            }

            // Проверяем на валидность
            this._isValidate(_result);

            return _result;
        },
        /**
         * Очистка значения control
         */
        clearResult: function () {
            this.set("_result", null);
            this.set("_result_id", null);
        },
        /**
         * Устанавливаем значение в control
         */
        setResult: function () {

            // 1 значение должно быть _result
            if (arguments[0]) {
                this.set("_result", arguments[0]);
            } else {
                this.set("_result", null);
            }
            // 2 значение должно быть id
            if (arguments[1]) {
                this.set("_result_id", arguments[1]);
            } else {
                this.set("_result_id", null);
            }
        },
        /**
         * Действие после изменение результата
         */
        watchResult: function (callback) {
            this.observe("_result", callback);
        },
        /**
         * Запуск валидации, если она указана
         * @param value {String} перечень функций валидации
         */
        _isValidate: function (value) {

            var _self = this,
                _validateFunctions = UtilsMixin.stringToArray(this.get("validate"));

            _validateFunctions.forEach(function (func) {
                if (func && _self[func]) {
                    _self[func](value);
                }
            })
        },
        oninit: function () {

            var _self = this;
            _self._super();

            var _browser = _self.findParent("browser");

            _self.observe("_result _result_id", function (newVal, oldVal, keypath) {

                switch (keypath) {
                    case "_result":
                        _browser._setContext(_self);
                        break;
                    case "_result_id":
                        _browser._setContext(_self);
                        break;
                    default:
                        throw new Error("Не имеет свойств _result");

                }
            });


        }
    }
});