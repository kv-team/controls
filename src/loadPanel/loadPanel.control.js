/**
 * @class LoadPanel
 * @classdesc Компонент загрузка
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.loadPanel", [
    "control",
    "controls.image",
    "controls.label"
], function (Control, Image, Label) {

    return function (options) {
        return Control.extend({
            isolated: true,
            template: "#kv-load-panel-control",
            components: {
                image: Image(),
                label: Label()
            },
            data: function () {
                return {};
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});
