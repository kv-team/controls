/**
 * @class Label
 * @classdesc Компонент текст
 * @extends Control
 *
 * @property value {String:"Default header"} Текст заголовка
 * @property icon {String:""} Иконка
 *
 * @author A Ivanov
 */
define("controls.label", [
    "control"
], function (Control) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-label-control",
            data: function () {
                return {
                    value: options && options.value || "Default",
                    icon: options && options.icon || ""
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});