/**
 * @class Menu
 * @classdesc Компонент меню
 * @extends Control
 * @extend OpenExtend
 *
 * @property logoUrl {String} Путь к логотипу
 * @property items {Array} Список пунктов меню слева
 * @property items_right {Array} Список пунктов меню справа
 *
 * @author A Ivanov
 */
define("controls.menu", [
    "control",
    "controls.linkControl",
    "controls.image",
    "controls.button",
    "controls.langControl",
    "mixin.openExtend"
], function (Control, LinkControl, Image, Button, LangControl, OpenExtend) {

    return function (options) {

        /**
         * Активация пунктов меню
         * @param $item {Object} Пункт меню
         */
        Control.prototype.activate = function ($item) {

            this.getDomInstance().find(".kv-active").removeClass("kv-active");

            // Если у данного пункта меню нет вложений разрешать выделение
            if ($item.find(".sub-menu").length === 0) {
                $item.addClass("kv-active");
            }

            $item.parents(".item").addClass("kv-active");

        };
        /**
         * Переформирование событий на пунктах меню
         * @private
         */
        Control.prototype._refreshLinkEvents = function () {
            var _self = this,
                $item_prior,
                $item_current,
                $sub_menu_prior_children,
                $sub_menu_prior_parent,
                $sub_menu_current_children,
                $sub_menu_current_parent;

            _self.findAllComponents("linkControl").forEach(function (link) {

                link.subscribe({
                    clickLink: function () {

                        $item_current = link.getDomInstance().parent(".item");
                        $sub_menu_current_children = $item_current.children(".sub-menu");

                        _self.showSubMenu($item_current);

                        // Если это не sub-menu
                        if ($sub_menu_current_children.length === 0) {
                            _self.getDomInstance().find(".sub-menu").hide();
                            _self.closeExtend();
                        }

                        _self.activate($item_current);
                    },
                    linkMouseOver: function () {

                        if (_self.jQuery(window).innerWidth() > 768) {

                            $item_current = link.getDomInstance().parent(".item");
                            $sub_menu_current_children = $item_current.children(".sub-menu");

                            _self.showSubMenu($item_current);

                            // если это подменю будет заполнено
                            $sub_menu_current_parent = $item_current.parent(".sub-menu");

                            // если нет дочерних подменю
                            if ($sub_menu_current_children.length === 0) {
                                $sub_menu_current_parent.find(".sub-menu").hide();
                            }

                            if ($sub_menu_current_parent.length === 0) {

                                if ($item_prior && $item_current) {
                                    if ($item_prior.children(".kv-link-control")[0] && $item_prior.children(".kv-link-control")[0].id
                                        !== $item_current.children(".kv-link-control")[0].id) {
                                        $item_prior.find(".sub-menu").hide();
                                        $item_prior.parents(".sub-menu").hide();
                                    }
                                }
                            }
                        }
                    }
                    ,
                    linkMouseOut: function () {
                        $item_prior = $item_current;
                        $sub_menu_prior_children = $sub_menu_current_children;
                        $sub_menu_prior_parent = $sub_menu_current_parent;
                    }
                });
            });
        };
        /**
         * Получение списка свернутых пунктов меню
         * @return {Array}
         */
        Control.prototype.getZipItems = function () {
            return this._.last(this.get("items"), this.get("_width_items_hidden").length);
        };
        /**
         * Схлопывание пуктов меню при масштабировании
         * @private
         */
        Control.prototype._collapseItems = function () {

            var _self = this,
                _right_left,
                $left_menu = _self.getDomInstance().find(".kv-menu-top-control__elem").first(),
                $right_menu = _self.getDomInstance().find(".kv-menu-top-control__elem").last(),
                $last = $left_menu.find(">.item").not(".kv-hidden").not(".zip-item").last();

            _self.toggleZipItem = function ($left_menu) {
                if ($left_menu.find(">.item.kv-hidden").not(".zip-item").length === 0) {
                    $left_menu.find(".zip-item").addClass("kv-hidden");
                } else {
                    $left_menu.find(".zip-item").removeClass("kv-hidden");
                }
            };

            _self.addItemHidden = function ($last) {
                this.push("_width_items_hidden", $last.outerWidth());
                $last.addClass("kv-hidden");
                this._refreshLinkEvents();
                return $left_menu.find(">.item").not(".kv-hidden").not(".zip-item").last();
            };

            _self.removeItemHidden = function () {
                this.pop("_width_items_hidden");
                var $first = $left_menu.find(">.item.kv-hidden").not(".zip-item").first();
                $first.removeClass("kv-hidden");
                return $first;
            };

            _self.getLastVisibleRightItem = function () {
                var _width_items_hidden = _self.get("_width_items_hidden");

                // Если есть скрытые блоки при разворачивании из мобильного меню в браузерное
                return $last.offset().left + $last.outerWidth() + 30;
            };

            _self.getFirstHiddenRightItem = function () {
                var _width_items_hidden = _self.get("_width_items_hidden");
                return this.getLastVisibleRightItem() + _width_items_hidden[_width_items_hidden.length - 1];
            };

            _self.changeItems = function () {

                // Позиция left у правого блока меню
                if (this.jQuery(window).outerWidth() > 768) {

                    _right_left = $right_menu.offset().left;

                    var _width_items_hidden = _self.get("_width_items_hidden");

                    if (_width_items_hidden.length > 0 && $left_menu.find(">.item.kv-hidden").not(".zip-item").length === 0) {
                        _self.jQuery(_self._.last($left_menu.find(">.item").not(".zip-item"), _width_items_hidden.length)).addClass("kv-hidden");
                    }

                    if ($last.length > 0 && $last.outerWidth() > 0) {
                        while (this.getLastVisibleRightItem() > _right_left) {
                            $last = _self.addItemHidden($last);
                        }

                        // Обработка скрытых пунктов меню
                        while (this.getFirstHiddenRightItem() < _right_left) {
                            $last = _self.removeItemHidden($last);
                        }
                    }
                } else {
                    $left_menu.find(">.item").not(".zip-item").removeClass("kv-hidden");
                }

                _self.toggleZipItem($left_menu);

            };

            // Инициализируем позицию
            _self.changeItems();

            // При масштабировании меню рассчитываем видимость блоков
            _self.jQuery(window).resize(function () {
                _self.changeItems();
            });
        };
        /**
         * Отображение блока подменю
         * @param $item {LinkControl} компонент ссылка
         */
        Control.prototype.showSubMenu = function ($item) {

            var _self = this,
                $sub_menu = $item.children(".sub-menu"),
                _z_index = _self.get("_z_index");

            _z_index++;

            if ($sub_menu.length > 0 && $sub_menu.css("display") !== "block") {

                var _right_item = ($item.parent(".sub-menu").length > 0) ? $sub_menu.innerWidth() * 2 : $sub_menu.innerWidth();

                _right_item += $item.offset().left;

                if (_self.jQuery(window).innerWidth() < _right_item) {

                    if ($item.parent(".sub-menu").length === 0 && $sub_menu.length > 0) {
                        $sub_menu.css("left", -$sub_menu.innerWidth() + 2 + $item.innerWidth());
                        $sub_menu.css("z-index", _z_index);
                    }

                    if ($item.parent(".sub-menu").length > 0) {
                        $sub_menu.css("left", -$sub_menu.innerWidth() - 2);
                        $sub_menu.css("top", -1);
                        $sub_menu.css("z-index", _z_index);
                    }
                } else {
                    if ($item.parent(".sub-menu").length > 0) {
                        $sub_menu.css("left", $sub_menu.innerWidth());
                        $sub_menu.css("top", -1);
                        $sub_menu.css("z-index", _z_index);
                    }
                }
                $sub_menu.show();
                _self.set("_z_index", _z_index);
            } else {
                if (_self.jQuery(window).outerWidth() <= 768) {
                    $sub_menu.find(".sub-menu").hide();
                    $sub_menu.hide();
                }
            }
        };

        return Control.extend({
            template: "#kv-menu-top-control",
            isolated: true,
            partials: {
                sub_menu: $("#kv-menu-item-control").html() || ""
            },
            components: {
                linkControl: LinkControl(),
                image: Image(),
                btnMobileMenu: Button(),
                langControl: LangControl()
            },
            data: function () {
                return {
                    logoUrl: options && options.logoUrl || "",
                    items: options && options.items || [],
                    items_right: options && options.items_right || [],
                    _z_index: 5,
                    visible_lang: options && options.visible_lang || false,
                    _width_items_hidden: []
                }
            },
            oninit: function () {

                var _self = this;

                _self.findComponent("btnMobileMenu").subscribe("clickBtn", function () {
                    _self.toggleExtend();
                });

                _self.watchExtend(function () {

                    if (_self.jQuery) {
                        var $elements = _self.getDomInstance().find(".kv-menu-top-control__elem");

                        if (_self.isOpen()) {
                            $elements.css("display", "flex");
                        } else {
                            $elements.hide();
                        }

                        _self.getDomInstance().find(".sub-menu").hide();
                    }
                });

                _self._super();
            },
            onrender: function () {
                var _self = this,
                    $elements = _self.getDomInstance().find(".kv-menu-top-control__elem");

                _self.jQuery(window).on({
                    resize: function () {

                        // Переключение режима отображения главного меню
                        if (_self.jQuery(window).width() > 768) {
                            $elements.css("display", "flex");
                            _self.closeExtend();
                        } else {
                            if (!_self.isOpen()) {
                                $elements.hide();
                            }
                        }
                    },
                    click: function (event) {

                        var $control = _self.jQuery(_self.jQuery(event.target).parents(_self.getCssId()));

                        if ($control.length === 0) {
                            _self.closeExtend();
                            _self.getDomInstance().find(".sub-menu").hide();
                        }
                    }
                });

                _self._collapseItems();
                _self._refreshLinkEvents();

                _self._super();
            }
        }, OpenExtend);
    }
});