var gutil = require('gulp-util'),
    config = require('../../config');

/**
 * Обработка ошибок при компиляции PUG
 * @param error
 */
module.exports = function (error) {

    config.blockNo('Compile PUG');
    gutil.log(gutil.colors.red("----------------------------------------------"));
    gutil.log(gutil.colors.blue("Message:"), gutil.colors.green(error.message));
    gutil.log(gutil.colors.red("----------------------------------------------"));

    this.emit('end');
};