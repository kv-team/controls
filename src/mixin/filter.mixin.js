/**
 * @class Filter
 * @classdesc Расширение для получения результатов фильтрации
 *
 * @property _autoFilter {boolean} Автоматическая фильтрация при изменении любого значения из фильтра.
 * @property _filtersValue {{}} Фильтруемые значения.
 *
 * @author A Ivanov
 */
define("mixin.filter", ["mixin.utils"], function (UtilsMixin) {

    return {
        /**
         * Записываем значения фильтров
         * @throws На странице отсутствует компонент Browser
         */
        _setFiltersValue: function () {

            var _browser = this.findParent("browser"),
                _filtersValue = {};

            if (_browser) {

                // 1. Получаем перечень значений фильтрации
                this.get("filters").forEach(function (name) {

                    if (_browser.UI(name).getResult && _browser.UI(name).getResult() !== null) {
                        _filtersValue[name] = _browser.UI(name).getResult();
                    }
                });

                // 2. Заполняем значение фильтров
                this.set("_filtersValue", _filtersValue);

            } else {
                console.error("На странице отсутствует компонент Browser");
            }
        },
        /**
         * Получаем значения фильтров
         * @return {{}}
         */
        getFiltersValue: function () {

            // Инициализируем фильтр (ручной режим)
            this._setFiltersValue();

            return this.get("_filtersValue") || {};
        },
        oninit: function () {
            var _self = this,
                _browser = _self.findParent("browser");

            // Автоматическое обновление _dataSource при изменении значения фильтра
            _self.set("_autoFilter", false);
            // Значения фильтров
            _self.set("_filtersValue", {});

            var _componentsName = _self.get("filters") || "",
                _names = [];

            if (_componentsName.length > 0) {
                _names = UtilsMixin.stringToArray(_componentsName).map(function (componentName) {
                    // Всвязи с тем что у нас вложенные компоненты то у них имя отличается
                    return (_browser.UI(componentName).getComponentName() === "fieldBtnControl") ? componentName + "__text" : componentName;
                });
            }

            _self.set("filters", _names);

            _self._super();

        }
    }
});