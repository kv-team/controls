/**
 * @class Mask
 * @classdesc Расширение для отображения маски.
 *
 * @property mask {string} метод маски (название функции)
 *
 * @author A Ivanov
 */
define("mixin.mask", [], function () {

    return {
        // -------------------------------------------------------------------------------------------------
        // Public method
        // -------------------------------------------------------------------------------------------------
        /**
         * Устанавливает маску для мобильного телефона
         * @example +7 (999) 999-99-99
         */
        isMaskMobilePhone: function () {
            //$("input", this.getCssId()).mask("+7 (999) 999-99-99");
        },
        data: function () {
            return {
                // Маска по умолчанию
                mask: ""
            }
        },
        oninit: function () {
            var _self = this;

            _self._super();
        },
        onrender: function () {
            var _self = this,
                _mask = this.get("mask");

            if (_mask && _mask.length > 0 && this[_mask]) {
                this[_mask]();
            }

            _self._super();
        }
    }
});