/**
 * @class Selected
 * @classdesc Расширение для выделения записей и получение информации по ним
 *
 * @author A Ivanov
 */
define("mixin.selected", [], function () {

    return {
        data: function () {
            return {
                /**
                 * Выделенные идентификаторы
                 * {Array:Integer}
                 */
                _selectIds: [],
                /**
                 * Текущий идентификатор
                 * {Integer}
                 */
                _currentId: null
            }
        },
        //----------------------------------------------------------------------------------------------------
        // Private methods
        //----------------------------------------------------------------------------------------------------
        _setCurrentId: setCurrentId,
        _setSelectedIds: setSelectedIds,
        //----------------------------------------------------------------------------------------------------
        // Public methods
        //----------------------------------------------------------------------------------------------------
        /**
         * Получение идентификатора записи на которой стоит указатель
         * @return {Integer}
         */
        getCurrentId: getCurrentId,
        /**
         * Получение массива индетификаторов выбранных записей
         * @return {Array:Integer}
         */
        getSelectedIds: getSelectedIds,

        //----------------------------------------------------------------------------------------------------
        // Events methods
        //----------------------------------------------------------------------------------------------------
        onClickCheckBox: function (event) {
            var _id = event.context.id,
                _selectedIds = this.getSelectedIds();

            if (event.node.checked === true) {
                _selectedIds.push(_id);
            } else {
                _selectedIds = _.difference(_selectedIds, [_id]);
            }

            this.set("_selectedIds", _selectedIds);
        },
        onClickRow: function (event) {
            this._setCurrentId(event.context.id);
        },
        oninit: function () {
            var _self = this;

            _self.subscribe({
                // Нажатие на ячейку
                /**
                 * Нажатие на строку в компоненте
                 * @event clickRow
                 */
                clickRow: _self.onClickRow,
                // Клик по чек-боксу
                /**
                 * Нажатие на галку, с помощью которой происходит множетвенная выборка
                 * @event clickCheckBox
                 */
                clickCheckBox: _self.onClickCheckBox
            });

            _self._super();
        }
    };

    /**
     * Получить идентификатор записи по которой осуществили нажатие
     * @return {null}
     */
    function getCurrentId() {
        return this.get("_currentId") || null
    }

    /**
     * Устанавливаем идентификатор записи по которой осуществили нажатие
     * @param val {integer} числовой идентификатор, соответсивует id из БД
     */
    function setCurrentId(val) {
        this.set("_currentId", val || null)
    }

    /**
     * Получение списка выделенных записей
     * @return {Array:Integer} id записей выбранных в компоненте
     */
    function getSelectedIds() {
        return this.get("_selectedIds") || []
    }

    /**
     * Сохраняем идентификаторы выбранных строк
     * @param ids {Array:Integer} перечень идентификаторов соответсвует id из БД
     */
    function setSelectedIds(ids) {
        this.set("_selectedIds", ids || [])
    }
});
