/**
 * @class Tabs
 * @classdesc Компонент панель для вкладок
 * @extends Control
 *
 * @property items {Array:String} Массив ссылок генерируется автоматически
 * @property tabDefault {String} Вкладка по умолчанию
 *
 * @author A Ivanov
 */
define("controls.tabs", [
    "control",
    "controls.tab",
    "controls.linkControl"
], function (Control, Tab, LinkControl) {

    return function (options) {

        /**
         * Открытие конкретной вкладки по name
         * @param tabName
         */
        Control.prototype.showTab = function (tabName, index) {

            if (tabName) {
                // Скрывает все табы и показываем нужный
                this.jQuery(".kv-tabs-control__tab", this.getCssId()).css("display", "none");
                this.jQuery("[name='" + tabName + "']", this.getCssId()).css("display", "block");
            }
            var _tab = this.findAllComponents("tab").filter(function (tab) {
                return tab.getName() === tabName;
            });
            this.jQuery(this.jQuery(".kv-tabs-control_link", ".kv-tabs-control__header")
                .get(_tab[0].getIndex())).addClass("kv-active");
        };

        return Control.extend({
            template: "#kv-tabs-control",
            components: {
                tab: Tab(),
                linkControl: LinkControl()
            },
            data: function () {
                return {
                    items: [],
                    tabDefault: ""
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {

                var _self = this,
                    _tabs = _self.findAllComponents("tab");

                _tabs.forEach(function (tab, idx) {
                    tab.set("index", idx);
                    _self.push("items", {caption: tab.getCaption()});

                    _self.findAllComponents("linkControl")[idx].subscribe("clickLink", function (event) {
                        if (!tab.get("url") || tab.get("url") === "") {
                            event.original.preventDefault();
                            _self.jQuery(".kv-tabs-control_link").removeClass("kv-active");
                            _self.showTab(tab.getName());
                        }
                    });
                });

                if (_self.get("tabDefault") && _self.get("tabDefault").length > 0) {
                    _self.showTab(_self.get("tabDefault"));
                } else {
                    _self.showTab(_self.findComponent("tab").getName());
                }

                _self._super();
            }
        });
    }
});