/**
 * @class room
 * @extends Control 
 */
define('controls.room', [
	'control',
], function (Control) {
	return function (options) {
		return Control.extend({
			isolated: true,
			template: '#kv-room-control',
			data: function () {
				return {
				}
			},
			oninit: function () {
				var _self = this;
                _self._super();
			},
			onrender: function () {
				var _self = this;
				_self._super();

				if (UnityLoader && UnityProgress) {
                    var gameInstance = UnityLoader.instantiate("gameContainer", "/js/3dRoomBuild.json", {
                        onProgress: UnityProgress
                    });
                } else {
					console.error("require UnityLoader or UnityProgress")
				}
			}
		})
	}
});