/**
 * @class Memo
 * @classdesc Поле ввода memo
 * @extends Control
 * @extend Resulted
 * @extend Validate
 *
 * @property placeholder {String} Подсказка в поле для ввода
 * @property validate {String} Метод проверки значения
 *
 * @author A Ivanov
 */
define("controls.memo", [
    "control",
    "mixin.resulted",
    "mixin.validate"
], function (Control, ResultedMixin, ValidateMixin) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-text-area-control",
            data: function () {
                return {
                    placeholder: options && options.placeholder || "Введите значение"
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        }, ResultedMixin, ValidateMixin);
    }
});