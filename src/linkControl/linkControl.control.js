/**
 * @class LinkControl
 * @classdesc Компонент ссылка
 * @extends Control
 *
 * @property value {String} Текст в ссылке
 * @property icon {String} Иконка из bootstrap
 * @property url {String} Ссылка по которой переходим по ссылке
 *
 * @author A Ivanov
 */
define("controls.linkControl", [
    "control"
], function (Control) {

    return function (options) {

        return Control.extend({
            template: "#kv-link-control",
            data: function () {
                return {
                    value: options && options.value || "Default link",
                    icon: options && options.icon || "",
                    url: options && options.url || ""
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});