var gutil = require('gulp-util'),
    config = require('../../config');

/**
 * Обработка ошибок при конкатинации JS
 * @param error
 */
module.exports = function (error) {

    config.blockNo('Concat js');
    gutil.log(gutil.colors.red("----------------------------------------------"));
    gutil.log(gutil.colors.blue("Message:"), gutil.colors.green(error.message));
    gutil.log(gutil.colors.red("----------------------------------------------"));

    this.emit('end');
};