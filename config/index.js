/**
 * Обработка конфигурации компонентов
 */
var yml = require('yamljs'),
    _ = require('underscore'),
    gutil = require('gulp-util'),
    logger = require('./logger'),
    path = require('path');

function Config() {
    this.common = yml.load(path.join(__dirname, 'common.yml')) || {};
}

/**
 * Путь к билду
 * @return {*|string}
 */
Config.prototype.dirBuild = function () {
    return this.common['dir']['build'] || '';
};

/**
 * Путь к public клиента
 * @return {*|string}
 */
Config.prototype.dirPublic = function () {
    return this.common['dir']['public'] || '';
};

/**
 * Путь к директории ноды
 * @return {*|string}
 */
Config.prototype.dirNode = function () {
    return this.common['dir']['node_module'] || '';
};

/**
 * Путь к директории с исходниками компонентов
 * @return {*|string}
 */
Config.prototype.dirSrc = function () {
    return this.common['dir']['src'] || '';
};

/**
 * Путь к директории с документацией
 * @return {*|string}
 */
Config.prototype.dirDocs = function () {
    return this.common['dir']['docs'] || '';
};

/**
 * Путь к директории с демонстрационной версией сайта
 * @return {*|string}
 */
Config.prototype.dirDemo = function () {
    return this.common['dir']['demo'] || '';
};

/**
 * Путь к директории с тестами
 * @return {*|string}
 */
Config.prototype.dirTest = function () {
    return this.common['dir']['test'] || '';
};


/*==================================
 Logger
 ==================================*/

/**
 * Сообщение при старте задачи
 * @return {*|string}
 */
Config.prototype.taskStart = function (name) {
    var _compiled = _.template(this.common['logger']['task_start'] || '');
    gutil.log(gutil.colors.green.bold(_compiled({name: name})));
};

/**
 * Сообщение при завершении задачи
 * @return {*|string}
 */
Config.prototype.taskEnd = function (name) {
    var _compiled = _.template(this.common['logger']['task_end'] || '');
    gutil.log(gutil.colors.green.bold(_compiled({name: name})));
};

/**
 * Сообщение при успешном выполнении блока
 * @return {*|string}
 */
Config.prototype.blockOk = function (name) {
    var _compiled = _.template(this.common['logger']['success'] || ''),
        _text = _compiled({name: gutil.colors.blue.bold(name)}) + gutil.colors.cyan('OK');
    logger.info(_text);
};

/**
 * Сообщение при ошибке в блоке
 * @return {*|string}
 */
Config.prototype.blockNo = function (name) {
    var _compiled = _.template(this.common['logger']['success'] || ''),
        _text = _compiled({name: gutil.colors.blue.bold(name)}) + gutil.colors.cyan('NO');
    logger.error(_text);
};

/**
 * Сообщение при ошибке в блоке
 * @return {*|string}
 */
Config.prototype.watchStart = function (name) {
    var _compiled = _.template(this.common['logger']['watch_start'] || '');
    gutil.log(gutil.colors.cyan(_compiled({name: gutil.colors.yellow.bold(name)})));
};

module.exports = new Config();