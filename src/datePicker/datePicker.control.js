/**
 * @class DatePicker
 * @classdesc Компонент календарь
 * @extends Control
 * @extend Resulted
 * @extend OpenExtend
 *
 * @author A Ivanov
 */
define("controls.datePicker", [
    "control",
    "datepicker",
    "controls.fieldBtnControl",
    "mixin.resulted",
    "mixin.openExtend"
], function (Control, $Datepicker, FieldBtnControl, ResultedMixin, OpenExtend) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-datepicker-control",
            components: {
                fieldBtnControl: FieldBtnControl()
            },
            data: function () {
                return {};
            },
            oninit: function () {

                var _self = this;

                _self.findComponent("fieldText").subscribe({
                    /**
                     * Нажатие на кнопку Escape
                     * @event keyDown
                     */
                    onKeyDown: function () {
                        if (this.event.original.key === "Backspace") {
                            this.clearResult(null, null);
                        }
                    }
                });

                /**
                 * Нажатие на кнопку раскрытия списка
                 * @event clickBtn
                 */
                _self.findComponent("fieldBtn").subscribe("clickBtn", function () {
                    var $datepicker = _self.jQuery(_self.jQuery(".datepicker", _self.getCssId()));

                    if (_self.isOpen() === true) {
                        _self.closeExtend();
                        $datepicker.css("display", "none");
                    } else {
                        _self.openExtend();
                        $datepicker.css("display", "block");
                    }
                });

                _self._super();
            },
            onrender: function () {

                var _self = this;

                _self.jQuery("[name='" + _self.getName() + "']").datepicker({
                    format: "dd.mm.yyyy"
                });

                var $datepicker = _self.jQuery(".datepicker", _self.getCssId());

                $datepicker[1].remove();

                $datepicker.on("click", "td", function () {

                    $datepicker.css("display", "none");
                });

                $datepicker.css("display", "none");

                // TODO решение временное. Необходимо подумать как лучше это переделать.
                _self.jQuery("input", _self.getCssId()).change(function () {

                    if (_self.jQuery(this).val() !== undefined) {
                        _self.findComponent("fieldText").setResult(_self.jQuery(this).val());

                    }
                });

                _self._super();
            }
        }, ResultedMixin, OpenExtend);
    }
});