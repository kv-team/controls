define("controls", [
    "ractive",
    "controls.browser",
    "components"
], function (Ractive, Browser, components) {

    return function () {
        var _app = this;
        _app._locales = {};
        _app._browser = null;

        Ractive.DEBUG = false;

        if (arguments[0]) {
            try {
                _app._locales = JSON.parse(arguments[0])
            } catch (err) {
                throw new Error("Не удалось распарсить локали");
            }
        }

        _app.instance = new Ractive({
            el: "#demo",
            template: "#demo-wrapper",
            components: {
                browser: Browser({
                    partials: arguments[1] || {},
                    locales: _app._locales,
                    components: components
                })
            }
        });

        _app._browser = _app.instance.findComponent("browser");

        _app.getBrowser = function () {
            return this._browser;
        };

        _app.getComponentByName = function (name) {
            return _app._browser.UI(name);
        };
    }
});
