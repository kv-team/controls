var gutil = require('gulp-util'),
    config = require('../../config');

/**
 * Обработка ошибок при копировании файлов в другой репозиторий
 * @param error
 */
module.exports = function (error) {

    config.blockNo('Move file');
    gutil.log(gutil.colors.red("----------------------------------------------"));
    gutil.log(gutil.colors.blue("Message:"), gutil.colors.green(error.message));
    gutil.log(gutil.colors.red("----------------------------------------------"));

    this.emit('end');
};