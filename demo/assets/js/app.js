require([
    'controls',
    'text!/locales/locales.json',
    'text!/forms/dialog1.html',
    'text!/forms/authDialog.html',
    'text!/tabs/tab_command.html',
    'text!/tabs/tab_investor.html',
    'text!/slides/slide_1.html',
    'text!/slides/slide_2.html',
    'text!/slides/slide_3.html',
    'text!/slides/slide_4.html',
    'text!/slides/slide_5.html',
    'text!/partials/portfolio.html',
    'text!/partials/services.html',
    'text!/partials/clients.html',
    'text!/partials/site.html',
    'text!/partials/company.html',
    'text!/partials/works.html',
    'text!/partials/info.html',
    'text!/partials/advantages.html',
    'text!/pages/home/index.html',
    'text!/pages/contacts/index.html',
    'text!/pages/authorization/index.html',
    'text!/pages/portfolio/index.html'
], function (App, locales) {

    var app = new App(locales, require('text!/locales/locales.json'), {
        portfolio: require('text!/partials/portfolio.html'),
        services: require('text!/partials/services.html'),
        clients: require('text!/partials/clients.html'),
        site: require('text!/partials/site.html'),
        company: require('text!/partials/company.html'),
        works: require('text!/partials/works.html'),
        info: require('text!/partials/info.html'),
        advantages: require('text!/partials/advantages.html')
    });
});