/**
 * @module Сборщик проекта
 * @author Иванов Александр Сергеевич
 * @date 21 марта 2017
 *
 */
var fs = require("fs"),
    path = require("path"),
    gulp = require("gulp"),
    browserSync = require("browser-sync"),
    gulpsync = require("gulp-sync")(gulp),
    config = require("../config");
// Подключаем таски
require("./tasks");

/**
 * Компилирование web компонентов. Полный цикл.
 */
gulp.task("build:components", gulpsync.sync([
    "build:components:js",
    "build:components:css",
    "build:components:html"
    //"build:docs"
]), function () {
});

/**
 * Автоматическая сборка компонентов и демо сайта
 */
gulp.task("watch:components", gulpsync.sync([
    "watch:components:less",
    "watch:components:js",
    "watch:components:pug",
    "watch:demo",
    //"watch:docs",
    "watch:requirejs",
    "browserSync"]), function () {
});

/**
 * Запуск сервера для демо сайта
 */
gulp.task("browserSync", function () {
    browserSync({
        server: {
            baseDir: ["./"],
            index: path.join(config.dirDemo(), "public/index.html"),
            routes: {
                "/node_modules": config.dirNode(),
                "/build": config.dirBuild(),
                "/img": path.join(config.dirDemo(), "public/img"),
                "/partials": path.join(config.dirDemo(), "public/partials"),
                "/pages": path.join(config.dirDemo(), "public/pages"),
                "/slides": path.join(config.dirDemo(), "public/slides"),
                "/forms": path.join(config.dirDemo(), "public/forms"),
                "/tabs": path.join(config.dirDemo(), "public/tabs"),
                "/js": path.join(config.dirDemo(), "public/js"),
                "/locales": path.join(config.dirDemo(), "public/locales"),
                "/css": path.join(config.dirDemo(), "public/css"),
                "/": path.join(config.dirDemo())
            }
        },
        port: 3002,
        open: true,
        notify: false,
        reloadDelay: 1000
    });
});