/**
 * @class DataGrid
 * @classdesc Компонент таблица
 * @extends Control
 * @extend Selected
 * @extend Filter
 * @extend Sorted
 * @extend Data
 * @extend Http
 *
 * @description
 *  Данный компонент предназначен для вывода данных с сервера в виде таблицы.
 *  Для хранения шаблонов ячеек реализована папка ./public/templates/cell
 *  У таблицы есть кнопка "Обновить" по умолчанию
 *  Так же в таблицу предустановлен набор кнопок экспорта данных (смотрите ExportBtns) и
 *  общих кнопок действий над записью (смотрите SharedBtns).
 *
 *  @property url {String} Путь по которому будет производиться запрос к серверу (POST)
 *  @property columns {Array} Массив который хранит в себе конфигурацию колонок таблицы
 *
 * @author A Ivanov
 */
define("controls.dataGrid", [
        "control",
        "mixin.selected",
        "mixin.filter",
        "mixin.data",
        "mixin.sorted",
        "mixin.http",
        "controls.button",
        "controls.toolbar",
        "controls.loadPanel",
        "controls.exportsBtn",
        "controls.sharedBtn"
    ], function (Control, SelectedMixin, FilterMixin, DataMixin, SortedMixin, HttpMixin,
                 Button, Toolbar, LoadPanel, ExportsBtn, SharedBtn) {

        return function (options) {

            var _partials = function () {
                var _partials = {};

                if (options && options.columns) {

                    options["columns"].forEach(function (value) {
                        if (value.cellTemplate) {
                            _partials[value.field] = value.cellTemplate
                        }
                    });
                }
                return _partials;
            }();

            return Control.extend({
                template: "#kv-table-control",
                isolated: true,
                partials: _partials || {},
                components: {
                    gridToolbar: Toolbar({
                        components: {
                            gridBtnRefresh: Button(),
                            exportsBtn: ExportsBtn({
                                isExcel: options && options.isExcel || false,
                                isWord: options && options.isWord || false,
                                isPdf: options && options.isPdf || false
                            }),
                            sharedBtn: SharedBtn()
                        }
                    }),
                    gridLoadPanel: LoadPanel()
                },
                data: function () {
                    return {
                        columns: options && options.columns || []
                    }
                },
                oninit: function () {
                    var _self = this;

                    _self.findComponent("gridBtnRefresh").subscribe("clickBtn", function () {
                        _self.refresh();
                    });

                    _self.refresh();

                    _self._super();
                },
                onrender: function () {
                    var _self = this;
                    _self._super();
                }
            }, SelectedMixin, FilterMixin, DataMixin, SortedMixin, HttpMixin);
        }
    }
);