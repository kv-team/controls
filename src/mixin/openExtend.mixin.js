/**
 * @class Open Extend
 * @classdesc Расширение для работы  блоком который будет открываться/скрываться
 *
 * @property _isOpen {boolean} Признак видимости блока. По умолчанию блок скрыт
 *
 * @author A Ivanov
 */
define("mixin.openExtend", [], function () {

    return {
        data: function () {
            return {
                _isOpen: false
            }
        },
        /**
         *  Состояние признака открытия
         *  @return {boolean}
         */
        isOpen: function () {
            return this.get("_isOpen");
        },
        /**
         * Изменение состояния в зависимости от текущего
         */
        toggleExtend: function () {
            this.isOpen() === true ? this.closeExtend() : this.openExtend();
        },
        /**
         * Открыть блок
         */
        openExtend: function () {
            this.set("_isOpen", true);
        },
        /**
         * Закрыть блок
         */
        closeExtend: function () {
            this.set("_isOpen", false);
        },
        watchExtend: function (callback) {
            this.observe("_isOpen", callback);
        },
        oninit: function () {
            var _self = this;
            _self._super();
        },
        onrender: function () {
            var _self = this;

            _self.jQuery(window).click(function (event) {
                var $control = _self.jQuery(_self.jQuery(event.target).parents(_self.getCssId())[0]);

                if ($control.attr("id") !== _self.getId()) {
                    _self.closeExtend();
                }
            });

            _self._super();
        }
    }
});
