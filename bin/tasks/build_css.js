/**
 * Данные задачи отвечают за обработку стилевых описаний
 *
 * @author Иванов Александр Сергеевич
 * @date 21 марта 2017
 */

var fs = require("fs"),
    path = require("path"),
    gulp = require("gulp"),
    gulpsync = require("gulp-sync")(gulp),
    clean = require("gulp-clean"),
    less = require("gulp-less"),
    cssmin = require("gulp-cssmin"),
    rename = require("gulp-rename"),
    concatCss = require("gulp-concat-css"),
    autoprefixer = require("gulp-autoprefixer"),
    stripCssComments = require("gulp-strip-css-comments"),
    watch = require("gulp-watch"),
    browserSync = require("browser-sync"),
    errors = require("../errors"),
    config = require("../../config");

/**
 *  Компилирование стилевых описаний для web компонентов
 */
gulp.task("build:components:css", gulpsync.sync([
    "clean:components:css",
    "compile:components:less"
]), function () {
});

/**
 * Удаление стилевых описаний у компонентов
 */
gulp.task("clean:components:css", function () {

    config.taskStart("clean:components:css");

    // Обновляем статистику по build components
    if (fs.existsSync(config.dirBuild())) {

        return gulp.src([
            path.join(config.dirBuild(), "**/*.css"),
            path.join(config.dirBuild(), "*.css")
        ])
            .pipe(clean({force: true})
                .on("data", function () {
                    config.blockOk("Clear files");
                })
                .on("error", errors.ClearError))
            .on("finish", function () {
                config.taskEnd("clean:components:css")
            });
    } else {
        fs.mkdirSync(config.dirBuild());
    }
});

/**
 * Компилирование less для компонентов
 */
gulp.task("compile:components:less", function () {

    config.taskStart("compile:components:less");

    return gulp.src(path.join(config.dirSrc(), "**/*.less"))
        .pipe(less()
            .on("data", function () {
                config.blockOk("Compile LESS");
            })
            .on("error", errors.LessError))
        .pipe(autoprefixer({
            browsers: ["last 5 versions"],
            cascade: false
        })
            .on("data", function () {
                config.blockOk("Autoprefix CSS");
            })
            .on("error", errors.PrefixError))
        .pipe(stripCssComments()
            .on("data", function () {
                config.blockOk("Strip comment CSS");
            })
            .on("error", errors.CommentCssError))
        .pipe(gulp.dest(config.dirBuild()))
        .pipe(cssmin()
            .on("data", function () {
                config.blockOk("Minimized CSS");
            })
            .on("error", errors.MinCssError))
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(config.dirBuild()))
        .pipe(concatCss("components.css")
            .on("data", function () {
                config.blockOk("Concat CSS");
            })
            .on("error", errors.ConcatCssError)
        )
        .pipe(gulp.dest(config.dirBuild()))
        .pipe(cssmin()
            .on("data", function () {
                config.blockOk("Minimize CSS")
            })
            .on("error", errors.MinCssError))
        .pipe(rename({suffix: ".min"})
            .on("data", function () {
                config.blockOk("Rename CSS")
            })
            .on("error", errors.RenameError))
        .pipe(gulp.dest(config.dirBuild()))
        .on("finish", function () {
            config.taskEnd("compile:components:less");
        })
        .pipe(browserSync.reload({stream: true}));
});

/**
 * Слушаем изменения в файлах стилевых описаний
 */
gulp.task("watch:components:less", function () {

    config.watchStart("watch:components:less");

    watch(path.join(config.dirSrc(), "**/*.less"), function () {

        gulp.start("build:components:css");
    });
});
