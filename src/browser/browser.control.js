/**
 * @class Browser
 * @classdesc Компонент браузер
 * Данный компонент является обязательным.
 * Первым делом при разработке страниц его необходимо добавить на форму.
 * Он необходим для консалидировании контекса страницы.
 *
 * Требования при разработке:
 * 1. Браузер должен быть один
 * 2. Все элементы, которые будут содержать значения должны содержать аттрибут name заполненным
 * @extends Control
 * @extend Utils
 *
 * @author A Ivanov
 */

define("controls.browser", [
    "control",
    "controls.scrollBtn",
    "mixin.utils",
    "text!/js/locales.json"
], function (Control, ScrollBtn, UtilsMixin, componentsLocales) {

    return function (options) {

        options = options || {};
        options.components = options.components || {};
        options.partials = options.partials || {};
        options.locales = options.locales || {};

        options.components["scrollBtn"] = ScrollBtn();

        Control.prototype.sendMessage = function (events) {
            var $msg = this.jQuery(this.jQuery('.event-message-wrapper')[0]);
            $msg.removeClass('alert-error alert-warning alert-success');
            switch (events.eventStatus) {
                case 'ERROR':
                    $msg.addClass('alert-error');
                    break;
                case 'WARN':
                    $msg.addClass('alert-warning');
                    break;
                case 'SUCCESS':
                    $msg.addClass('alert-success');
                    break;
            }
            $msg.empty();
            $msg.append("<p>"+ events.message +"</p>");
            $msg.removeClass('kv-hidden');

            setTimeout(function () {
                $msg.addClass('kv-hidden');
                $msg.empty();
            }, 3000);
        };

        /**
         * Инициализация компонентов из localStorage
         * @throw Найдено несколько компонентов с одинаковым названием
         */
        Control.prototype._initContext = function () {

            var _self = this,
                _context = JSON.parse(localStorage.getItem(this.getName()));

            _self._.each(_context, function (item, componentTag) {
                // Ищим все компоненты по тегу
                var _components = _self.findAllComponents(componentTag);

                // Перебираем компоненты по группам
                _self._.each(_context[componentTag], function (item, componentName) {

                    // Отбираем конкретный компонент
                    var _component = _self._.compact(_self._.map(_components, function (component) {

                        if (component.getName() === componentName) {
                            return component;
                        }
                    }));

                    if (_component.length > 1) {
                        throw new Error("Найдено несколько компонентов с одинаковым названием: " + componentName);

                    } else if (_component.length > 0) {

                        _component[0].setResult(
                            _context[componentTag][componentName].value,
                            _context[componentTag][componentName].key
                        );
                    }
                })
            });
        };

        /**
         * Метод получения всего контекста страницы
         * @return {object} Выдает контекс компонента
         */
        Control.prototype.getContext = function () {
            return this.get("_context") || {};
        };

        /**
         * Метод изменения параметров контекста
         * @param component {Object} Наименование browser
         * @param name {String} Имя компонента
         * @param key {String} Название типа (id || value)
         * @param val {Object} Значение параметра
         * @throw Не указано название browser
         */
        Control.prototype._setContext = function (component) {

            if (this._.isUndefined(component)) {
                throw new Error("Не указан компонент");
            }

            var _obj = this.get("_context") || {},
                browser = this.getName();

            if (this._.isUndefined(browser)) {
                throw new Error("Не указано название browser");
            }

            var componentTagName = component.getComponentName(),
                componentName = component.getName();

            _obj[componentTagName] = _obj[componentTagName] || {};
            _obj[componentTagName][componentName] = _obj[componentTagName][componentName] || {};

            if (component.getResult() === "null" || component.getResult() === null) {
                delete _obj[componentTagName][componentName];
            } else {
                if (component.get('_result_id'))
                    _obj[componentTagName][componentName]["key"] = component.get('_result_id');
                if (component.get('_result'))
                    _obj[componentTagName][componentName]["value"] = component.get('_result');
            }

            // Удаляем компонент если он пуст
            if (Object.keys(_obj[componentTagName]).length === 0) {
                delete _obj[componentTagName];
            }

            this.set("_context", _obj || {});

            if (Object.keys(_obj).length > 0) {
                // TODO подумать о шифровании и вообще нужно ли оно
                localStorage.setItem(browser, JSON.stringify(this.getContext()));
            } else {
                localStorage.removeItem(browser);
            }
        };

        /**
         * Получение компонента по его наименованию
         * @param selector {String} Имя компонента
         * @return {Object} Возвращает найденный компонент
         * @throw Не указан selector
         */
        Control.prototype.UI = function (selector) {

            if (selector && selector.length > 0 && this.get("UI")[selector]) {
                return this.get("UI")[selector];
            } else {
                throw new Error("Not found component - " + selector);
            }
        };

        /**
         * Инициализируем UI в browser
         * @param component {this} - экземпляр компонента
         */
        Control.prototype.setUI = function (component) {

            var _UI = this.get("UI");

            if (_UI && component.getName && component.getName()) {
                _UI[component.getName()] = component;
                this.set("UI", _UI);
            }
        };

        /**
         * Устанавливаем локаль
         * @param locale {String}
         */
        Control.prototype.setLocale = function (locale) {
            this.set("_defaultLocale", locale);
            localStorage.setItem("currentLocale", locale);
        };

        /**
         * Получение локали по ключу
         * @param key {String}
         */
        Control.prototype.t = function (key) {

            if (this.getValueById) {
                return this.getValueById(this.getLocales(), this.get("_defaultLocale").substring(0,2) + "." + key);
            } else {
                var _browser = this.findParent("browser");
                return _browser.getValueById(_browser.getLocales(), _browser.get("_defaultLocale") + "." + key);
            }
        };

        /**
         * Инициализация локалей
         * @param {Object} clientLocales - {JSON} Локали загруженные из файла locales.json
         */
        Control.prototype.initLocales = function (clientLocales) {
            /**
             * Подгружаем локали с клиента
             */
            this.set("_locales", clientLocales);
            /**
             * Устанавливаем язык по умолчанию (берем из браузера)
             */
            this.set("_defaultLocale", localStorage.getItem("currentLocale") || navigator.language);
        };

        /**
         * Получение списка локалей в виде JSON объекта
         */
        Control.prototype.getLocales = function () {
            return this.get("_locales") || {};
        };

        return Control.extend({
            isolated: true,
            template: "#kv-browser-control",
            components: options && options.components,
            partials: options && options.partials,
            data: function () {
                return {
                    _context: {},
                    UI: {},
                    _locales: {},
                    _defaultLocale: ""
                }
            },
            onconfig: function () {
                var _self = this;
                _self.initLocales(_self.mergeJSON(JSON.parse(componentsLocales), options.locales));
                _self._super();
            },
            oninit: function () {
                var _self = this;

                /** инициализация контекста */
                _self._initContext();
                _self._super();
            }
        }, UtilsMixin);
    }
});