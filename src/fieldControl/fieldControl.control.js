/**
 * @class FieldControl
 * @classdesc Компонент для ввода значения
 * @extends Control
 * @extend Resulted
 * @extend Validate
 * @extend Mask
 *
 * @property placeholder {String} Подсказка для пользователя
 * @property validate {String} Метод валидации из Validate
 * @property mask {String} Метод маскирования из Mask
 * @property text {String} Имя поля из БД (обязательно для заполнения)
 * @property type {string} Тип поля ввода
 *
 * @author A Ivanov
 */
define("controls.fieldControl", [
    "control",
    "mixin.resulted",
    "mixin.validate",
    "mixin.mask"
], function (Control, ResultedMixin, ValidateMixin, MaskMixin) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-field-control",
            magic: true,
            components: options && options.components || {},
            data: function () {
                return {
                    placeholder: options && options.placeholder || "Укажите значение",
                    validate: options && options.validate || "",
                    type: options && options.type || "text"
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        }, ResultedMixin, ValidateMixin, MaskMixin);
    }
});