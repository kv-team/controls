var fs = require('fs'),
    path = require('path'),
    errorHandlers = {};

fs.readdirSync(__dirname).filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js');
}).forEach(function (file) {
    errorHandlers[file.replace('Handler.js', '')] = require(path.join(__dirname, file));
});

module.exports = errorHandlers;


