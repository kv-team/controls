var gutil = require('gulp-util'),
    config = require('../../config');

/**
 * Обработка ошибок при установке вендорных префиксов
 * @param error
 */
module.exports = function (error) {

    config.blockNo('Autoprefixer CSS');
    gutil.log(gutil.colors.red("----------------------------------------------"));
    gutil.log(gutil.colors.blue("Message:"), gutil.colors.green(error.message));
    gutil.log(gutil.colors.red("----------------------------------------------"));

    this.emit('end');
};