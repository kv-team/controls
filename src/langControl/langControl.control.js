/**
 * @class langControl
 * @classdesc Компонент с языковой панелью
 * @extends Control
 *
 * @property _languages {array} перечень языков
 */
define("controls.langControl", [
    "control",
    "controls.label"
], function (Control, Label) {

    return function (options) {
        return Control.extend({
            isolated: true,
            template: "#kv-lang-control",
            components: {
                label: Label()
            },
            data: function () {
                return {
                    _languages: [{value: "ru"}, {value: "en"}]
                }
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;

                _self.get("_languages").forEach(function (lang) {
                    _self.jQuery("[name='" + _self.getName() + "_" + lang.value +"']", _self.getCssId()).on("click", function () {
                        _self.findParent("browser").setLocale(lang.value);
                    });
                });
                _self._super();
            }
        });
    }
});