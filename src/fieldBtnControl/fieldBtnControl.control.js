/**
 * @class FieldBtnControl
 * @classdesc Панель ввода с кнопкой
 * @extends Control
 * @extend OpenExtend
 *
 * @author A Ivanov
 */
define("controls.fieldBtnControl", [
    "control",
    "controls.button",
    "controls.fieldControl",
    "mixin.openExtend"
], function (Control, Button, FieldControl, OpenExtend) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-field-btn-control",
            components: {
                fieldText: FieldControl({
                    placeholder: options && options.placeholder || ""
                }),
                fieldBtn: Button({
                    icon: options && options.icon || "search"
                })
            },
            data: function () {
                return {
                    placeholder: options && options.placeholder || "Введите значение",
                    btnValue: options && options.btnValue || "",
                    // Признак наличия расширенного фильтра (вкл видимость кнопки)
                    _extended: options && options.extended || false
                }
            },
            oninit: function () {
                var _self = this;

                _self.subscribe({
                    onBtnExtClick: function () {
                        _self.toggleExtend();
                    }
                });

                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        }, OpenExtend);
    }
});