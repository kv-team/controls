var fs = require("fs"),
    path = require("path"),
    gulp = require("gulp"),
    gulpsync = require("gulp-sync")(gulp),
    clean = require("gulp-clean"),
    stripJsComments = require("gulp-strip-comments"),
    concat = require("gulp-concat"),
    minify = require("gulp-minify"),
    watch = require("gulp-watch"),
    less = require("gulp-less"),
    browserSync = require("browser-sync"),
    jsonMinify = require("gulp-jsonminify"),
    cssmin = require("gulp-cssmin"),
    replace = require("gulp-replace"),
    rename = require("gulp-rename"),
    autoprefixer = require("gulp-autoprefixer"),
    pugConverter = require("gulp-pug"),
    stripCssComments = require("gulp-strip-css-comments"),
    config = require("../../config");

var PUBLIC_PATH = "../public",
    PUBLIC_JS_PATH = PUBLIC_PATH + "/js",
    PUBLIC_CSS_PATH = PUBLIC_PATH + "/css",
    PUBLIC_IMG_PATH = PUBLIC_PATH + "/img",
    PUBLIC_FONT_PATH = PUBLIC_PATH + "/fonts",
    PUBLIC_LOCAL_PATH = PUBLIC_PATH + "/locales",
    PUBLIC_PARTIAL_PATH = PUBLIC_PATH + "/partials",
    PUBLIC_FORM_PATH = PUBLIC_PATH + "/forms",
    PUBLIC_TAB_PATH = PUBLIC_PATH + "/tabs",
    PUBLIC_PAGE_PATH = PUBLIC_PATH + "/pages",
    PUBLIC_SLIDE_PATH = PUBLIC_PATH + "/slides",
    VIEWS_PATH = "../views",
    ASSETS_PATH = "../assets",
    BUILD_PATH = "../../build";


gulp.task("build:js", function () {
    return gulp.src(path.join(ASSETS_PATH, "js/*.js"))
        .pipe(stripJsComments())
        .pipe(gulp.dest(PUBLIC_JS_PATH))
        .pipe(minify({
            ext: {
                min: ".min.js"
            }
        }))
        .pipe(gulp.dest(PUBLIC_JS_PATH))
});
gulp.task("build:css", function () {
    return gulp.src(path.join(ASSETS_PATH, "css/style.less"))
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ["last 5 versions"],
            cascade: false
        }))
        .pipe(stripCssComments())
        .pipe(gulp.dest(PUBLIC_CSS_PATH))
        .pipe(cssmin())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(PUBLIC_CSS_PATH));

});
gulp.task("link:components:html", function () {
    return gulp.src(path.join(BUILD_PATH, "components.html"))
        .pipe(gulp.dest(PUBLIC_PATH));
});
gulp.task("link:components:js", function () {
    return gulp.src([
        path.join(BUILD_PATH, "components.js"),
        path.join(BUILD_PATH, "**/*.js"),
        path.join(BUILD_PATH, "locales.json")
    ])
        .pipe(rename({dirname: ""}))
        .pipe(gulp.dest(PUBLIC_JS_PATH));
});
gulp.task("link:components:css", function () {
    return gulp.src(path.join(BUILD_PATH, "components.css"))
        .pipe(gulp.dest(PUBLIC_CSS_PATH));
});
gulp.task("build:img", function () {
    return gulp.src(path.join(ASSETS_PATH, "img/*"))
        .pipe(gulp.dest(PUBLIC_IMG_PATH));
});
gulp.task("build:libs", function () {
    return gulp.src(path.join(ASSETS_PATH, "libs/*.js"))
        .pipe(gulp.dest(PUBLIC_JS_PATH));
});
gulp.task("build:fonts", function () {
    return gulp.src(path.join(ASSETS_PATH, "fonts/*"))
        .pipe(gulp.dest(PUBLIC_FONT_PATH));
});
gulp.task("build:locales", function () {
    return gulp.src(path.join(ASSETS_PATH, "locales/*.json"))
        .pipe(jsonMinify())
        .pipe(gulp.dest(PUBLIC_LOCAL_PATH));
});
gulp.task("build:pug", function () {
    return gulp.src(path.join(VIEWS_PATH, "*.pug"))
        .pipe(pugConverter())
        .pipe(gulp.dest(PUBLIC_PATH));
});
gulp.task("build:partials", function () {
    return gulp.src(path.join(VIEWS_PATH, "partials/*.pug"))
        .pipe(pugConverter())
        .pipe(gulp.dest(PUBLIC_PARTIAL_PATH));
});
gulp.task("build:slides", function () {
    return gulp.src(path.join(VIEWS_PATH, "slides/*.pug"))
        .pipe(pugConverter())
        .pipe(gulp.dest(PUBLIC_SLIDE_PATH));
});
gulp.task("build:tabs", function () {
    return gulp.src(path.join(VIEWS_PATH, "tabs/*.pug"))
        .pipe(pugConverter())
        .pipe(gulp.dest(PUBLIC_TAB_PATH));
});
gulp.task("build:forms", function () {
    return gulp.src(path.join(VIEWS_PATH, "forms/*.pug"))
        .pipe(pugConverter())
        .pipe(gulp.dest(PUBLIC_FORM_PATH));
});
gulp.task("build:pages", function () {
    return gulp.src(path.join(VIEWS_PATH, "pages/**/*.pug"))
        .pipe(pugConverter())
        .pipe(gulp.dest(PUBLIC_PAGE_PATH));
});

gulp.task("html", function () {
    return gulp.src([
        path.join(PUBLIC_PATH, "index.html"),
        path.join(PUBLIC_PATH, "components.html")
    ])
        .pipe(concat("index.html"))
        .pipe(gulp.dest(PUBLIC_PATH));
});

gulp.task("build", gulpsync.sync([
    "link:components:html",
    "link:components:js",
    "link:components:css",
    "build:js",
    "build:css",
    "build:img",
    "build:partials",
    "build:pug",
    "build:forms",
    "build:tabs",
    "build:slides",
    "build:pages",
    "build:fonts",
    "build:locales",
    "build:libs",
    "html"
]), function () {

});
