var fs = require('fs'),
    bunyan = require('bunyan'),
    bformat = require('bunyan-format'),
    RotatingFileStream = require('bunyan-rotating-file-stream');

function Logger() {

    this.logger = null;

    this.initialize();
}

Logger.prototype.initialize = function () {

    var formatOut = bformat({
        outputMode: 'short'
    });

    if (!fs.existsSync('../logs')) {
        fs.mkdirSync('../logs');
    }

    this.logger = bunyan.createLogger({
        name: 'CONTROLS',
        streams: [{
            stream: new RotatingFileStream({
                totalSize: '20m',
                path: '../logs/client-debug.log',
                rotateExisting: true,
                totalFiles: 10,
                period: '1d',
                threshold: '10m',
                gzip: true
            })
        }, {
            stream: formatOut
        }],
        level: 'info'
    });
};

Logger.prototype.info = function (values) {
    this.logger.info(values);
};

Logger.prototype.warn = function (values) {
    this.logger.warn(values);
};

Logger.prototype.error = function (values) {
    this.logger.error(values);
};

module.exports = new Logger();