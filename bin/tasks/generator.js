var fs = require('fs'),
    config = require('../../config'),
    path = require('path'),
    gulp = require('gulp'),
    _ = require('underscore'),
    yml = require('yamljs'),
    args = require('yargs').argv;

/**
 * Генератор создания нового компонента
 * @example gulp generate:control --name ComponentName
 */
gulp.task('generate:control', function () {

    config.taskStart('generate:control');

    var regExp = /^[a-z,A-Z]+$/,
        _componentName = args.name;

    if (_.isString(_componentName)) {

        if (regExp.exec(_componentName) && _componentName.toLowerCase() !== 'control') {

            config.blockOk('Valid name');

            new Component(_componentName);

        } else {

            config.blockNo('Invalid name');
        }
    } else {
        config.blockNo('Missing name');
    }
});

/**
 * Иниуиализация модели компонента
 * @param name
 * @constructor
 */
function Component(name) {
    // Название компонента и его репозитория
    this.name = name.replace('Control', '');
    this.dirComponent = path.join(config.dirSrc(), this.name);

    // Если это новый репозиторий. Иначе еть риск затереть старый код
    if (this.initDir() === true) {
        this.initLESS();
        this.initPUG();
        this.initJS();
        this.initRequire();
    }
}

/**
 * Инициализируем репозиторий для нового компонента
 * @param name
 */
Component.prototype.initDir = function () {

    if (!fs.existsSync(this.dirComponent)) {

        fs.mkdirSync(this.dirComponent);
        config.blockOk('Dir init');
        return true;
    } else {
        config.blockNo('Dir exists');
        return false;
    }
};

/**
 * Инициализируем стилевое описание
 */
Component.prototype.initLESS = function () {

    fs.writeFile(path.join(this.dirComponent, this.name + '.less'),
        "@import '../_color';\n@import '../_mixin';\n\n.kv-" + this.name + "-control {\n\n}", function (err) {
            (err) ? config.blockNo('LESS') : config.blockOk('LESS');
        });
};

/**
 * Инициализируем модуль
 */
Component.prototype.initJS = function () {

    fs.writeFile(path.join(this.dirComponent, this.name + '.control.js'),
        "/**\n * @class " + this.name + "\n * @extends Control \n */\n" +
        "define('controls." + this.name + "', [\n\t'control',\n], function" +
        " (Control) {\n\treturn function (options) {\n\t\treturn Control.extend({\n\t\t\t" +
        "isolated: true,\n\t\t\ttemplate: '#kv-" + this.name + "-control',\n\t\t\tdata: function () {\n\t\t\t\treturn {\n\t\t\t\t}\n\t\t\t},\n\t\t\toninit: function () {\n\t\t\t\tvar _self = this;\n\t\t\t\t_self._super();" +
        "\n\t\t\t},\n\t\t\tonrender: function () {\n\t\t\t\tvar _self = this;\n\t\t\t\t_self._super();\n\t\t\t}\n\t\t})\n\t}\n});", function (err) {
            (err) ? config.blockNo('JS') : config.blockOk('JS');
        });

};

/**
 * Инициализируем шаблон
 */
Component.prototype.initPUG = function () {

    fs.writeFile(path.join(this.dirComponent, this.name + '.pug'),
        'script#kv-' + this.name + '-control(type="type/ractive")\n\t.kv-' + this.name + '-control(id="<{{._id}}>" class="<{{.class}}>" name="<{{.name}}>")\n\n', function (err) {
            (err) ? config.blockNo('PUG') : config.blockOk('PUG');
        });
};

/**
 * Подлкючаем компонент к requireJS
 */
Component.prototype.initRequire = function () {

    var _data = yml.load('../config/require.config.yml');
    _data['paths']['controls.' + this.name] = this.name + '/' + this.name + '.control.min';

    fs.writeFile('../config/require.config.yml', yml.dump(_data), function (err) {
        (err) ? config.blockNo('RequireJS') : config.blockOk('RequireJS');
    });
};