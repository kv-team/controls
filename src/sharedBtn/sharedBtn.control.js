/**
 * @class SharedButtons
 * @classdesc Кнопки с основными действиями над записями
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.sharedBtn", [
    "control",
    "controls.button",
    "controls.dialog"
], function (Control, Button, Dialog) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-shared-btn-control",
            components: {
                button: Button(),
                dialog: Dialog()
            },
            data: function () {
                return {
                    urlAdd: options && options.urlAdd || "",
                    urlEdit: options && options.urlEdit || ""
                }
            },
            oninit: function () {
                var _self = this;

                _self.findAllComponents("sharedBtn").forEach(function (btn) {
                    btn.subscribe("clickBtn", function (event) {

                        var _dialog =  _self.findComponent("dialog");

                        // Кнопка добавление
                        if (event.context.name.indexOf("shared_add") > 0 && _dialog) {
                            _dialog.set("caption", "{{ @this.t('sharedBtn.dialog_add_caption') }}");
                            _dialog.set("btnOk", "{{ @this.t('sharedBtn.btn_add_caption') }}");
                            _dialog.set("url", _self.get("urlAdd") || "");
                            _dialog.show();
                        }

                        // Кнопка добавление
                        if (event.context.name.indexOf("shared_edit") > 0 && _dialog) {
                            _dialog.set("caption", "{{ @this.t('sharedBtn.dialog_edit_caption') }}");
                            _dialog.set("btnOk", "{{ @this.t('sharedBtn.btn_save_caption') }}");
                            _dialog.set("url", _self.get("urlEdit") || "");
                            _dialog.show();
                        }
                    });
                });

                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});
