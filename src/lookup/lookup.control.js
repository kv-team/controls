/**
 * @class Lookup
 * @classdesc Выпадающий список
 * @extends Control
 * @extend Data
 * @extend Resulted
 * @extend Filter
 * @extend Http
 * @extend OpenExtend
 *
 * @property items {Array:Object} Коллекция данных
 * @property text {String:"name"} Колонка из БД которая отображает наименование
 * @property url {String} Ссылка для запроса данных с с сервера
 * @property placeholder {String} Подсказка внутри поля для ввода
 * @property _isOpen {boolean} Признак открытого списка
 *
 * @throws Не указана колонка с наименованием в компоненте
 *
 * @author A Ivanov
 */
define("controls.lookup", [
    "control",
    "controls.fieldBtnControl",
    "controls.loadPanel",
    "mixin.resulted",
    "mixin.data",
    "mixin.filter",
    "mixin.http",
    "mixin.openExtend",
    "mixin.validate",
], function (Control, FieldBtnControl, LoadPanel, ResultedMixin, DataMixin,
             FilterMixin, HttpMixin, OpenExtend, ValidateMixin) {

    return function (options) {

        return Control.extend({
            isolated: true,
            template: "#kv-lookup-control",
            components: {
                fieldBtnControl: FieldBtnControl({
                    icon: "menu-down",
                    btnValue: ""
                }),
                loadPanel: LoadPanel()
            },
            data: function () {
                return {
                    placeholder: options && options.placeholder || "Выберите значение",
                    text: options && options.text || "name",
                    key: options && options.key || "id",
                    items: []
                }
            },
            oninit: function () {
                var _self = this;

                if (_self.get("text") === "name") {
                    console.info("Не указана колонка с наименованием в компоненте " + _self.getName());
                }

                _self.clearResult();
                _self.refresh(_self.get("key") + "," + _self.get("text"));

                _self.findComponent("fieldText").subscribe({
                    /**
                     * Нажатие на кнопку Escape
                     * @event keyDown
                     */
                    onKeyDown: function () {
                        if (this.event.original.key === "Backspace") {
                            // Сбрасываем результат
                            this.setResult(null, null);
                        }
                    }
                });

                /**
                 * Нажатие на кнопку раскрытия списка
                 * @event clickBtn
                 */
                _self.findComponent("fieldBtn").subscribe("clickBtn", function () {
                    _self.toggleExtend();
                });

                _self.subscribe({
                    /**
                     * Нажатие на строку списка
                     * @event clickItem
                     */
                    clickItem: function (event) {
                        _self.closeExtend();
                        _self.findComponent("fieldText").setResult(
                            event.context[_self.get("text")],
                            event.context[_self.get("key")]
                        );
                    }
                });

                _self._super();
            },
            onrender: function () {

                var _self = this,
                    $dropdown = _self.jQuery(".kv-dropdown", _self.getCssId()),
                    $btn = _self.jQuery(_self.jQuery(".glyphicon", _self.getCssId())[0]);

                // Действия при открытии / закрытии выпадающей панели
                _self.watchExtend(function (newVal) {

                    $btn.removeClass("glyphicon-menu-up");
                    $btn.removeClass("glyphicon-menu-down");

                    $dropdown.toggleClass("kv-hidden");

                    if (newVal === true) {
                        $btn.addClass("glyphicon-menu-up");
                    } else {
                        $btn.addClass("glyphicon-menu-down");
                    }
                });

                // Инициализируем прокрутку области
                _self._initScroll($dropdown);

                _self._super();
            }
        }, ResultedMixin, DataMixin, FilterMixin, HttpMixin, OpenExtend, ValidateMixin);
    }
});