/**
 * @class Dialog
 * @classdesc компонент Диалог/Модальный диалог
 * @extends Control
 * @extend Http
 *
 * @property modal {boolean|false} Признак типа диалога. Если true то модальный
 * @property caption {String:"Default title"} Заголовок диалога
 * @property btnOk {String:"Ok"} Заголовок для кнопки отправить
 * @property _controlsResult {String} Компоненты которые будут анализироваться в диалоге
 * @property _alerts {String} Перечень классов CSS которые будут нужны при проверке
 *
 * @throws Диалог простой метод использовать нельзя
 *
 * @author A Ivanov
 */
define("controls.dialog", [
    "control",
    "controls.button",
    "controls.label",
    "mixin.http"
], function (Control, Button, Label, HttpMixin) {

    return function (options) {

        /**
         *  Скрыть диалог
         */
        Control.prototype.hide = function () {
            if (this.get("modal") === true) {
                var _$dialog = this.getDomInstance();

                if (!_$dialog.hasClass("kv-hidden")) {
                    _$dialog.addClass("kv-hidden");
                    this.jQuery(".kv-modal-shadow").remove();
                }
            } else {
                console.error("Диалог простой метод использовать нельзя")
            }
        };

        /**
         *  Показать диалог
         */
        Control.prototype.show = function () {
            if (this.get("modal") === true) {
                var _$dialog = this.getDomInstance();

                if (_$dialog.hasClass("kv-hidden")) {
                    _$dialog.removeClass("kv-hidden");
                    this.jQuery("body").append("<div class='kv-modal-shadow'></div>");
                }
            } else {
                console.error("Диалог простой метод использовать нельзя")
            }
        };

        /**
         * Проверка на возможность отправить значения
         * @return {boolean} Признак можно ли отправить данные на сервер
         */
        Control.prototype.isOk = function () {

            var _self = this;

            var _components = _self.findAllComponents().filter(function (component) {
                return !_self._.isUndefined(component.get("_empty")) && component.get("_empty") === false
                    && component.getResult && component.getResult() === "null";
            });

            return _components.length <= 0;
        };

        /**
         * Получение значений формы
         * @return {{}}
         */
        Control.prototype.getRequestParams = function () {
            var _self = this,
                _requests = {};

            _self.get("_controlsResult").forEach(function (_componentName) {
                var _components = _self.findAllComponents(_componentName);

                _components.forEach(function (component) {
                    if (component.getResult && component.getResult() !== null) {
                        _requests[component.getName()] = component.getResult();
                    }
                });
            });

            if (_self.getDomInstance().find('.g-recaptcha').length > 0 && grecaptcha) {
                _requests["g-recaptcha-response"] = grecaptcha.getResponse(_self.get("captcha_id"));
            }

            return _requests;
        };

        return Control.extend({
            template: "#kv-dialog-control",
            components: {
                dialogTitle: Label(),
                dialogBtnEnter: Button(),
                dialogBtnCancel: Button()
            },
            data: function () {
                return {
                    caption: options && options.caption || "Default title",
                    reload: options && options.reload || false,
                    // Тип диалога, по умолчанию простой диалог
                    modal: options && options.modal || false,
                    btnOk: options && options.btnOk || "Ok",
                    sitekey: options && options.sitekey || "6LdR2CcUAAAAAJTl_4JI0-6LB184vVbuMkrWI1IA",
                    // Компоненты у которых будут выбираться значения в диалоге
                    _controlsResult: ["fieldControl", "memo", "checkBox", "lookup"],
                    // Предупреждения при наличии которых не отправляем на сервер
                    _alerts: ["kv-alert-warning", "kv-alert-error"]
                }
            },
            oninit: function () {

                var _self = this;

                /**
                 * Событие нажатия на кнопку отправки
                 * @event clickBtnEnter
                 */
                _self.findComponent("dialogBtnEnter").subscribe("clickBtn", function () {

                    /** 1. Получим текущие значения формы */
                    var _results = _self.getRequestParams();

                    /** 2. Убеждаемся что данные можно отправить на сервер */
                    if (_self.isOk() === true) {

                        _self.$post(_results, function (resp) {

                            if (_self.get("modal") === true) {
                                _self.hide();
                            } else {
                                if (_self.get("reload") === true) {
                                    if (resp.redirect) {
                                        window.location.href = window.location.origin + resp.redirect_url;
                                    } else {
                                        if (!resp.status) {
                                            window.location.reload();
                                        } else {
                                            if (_self.getDomInstance().find('.g-recaptcha').length > 0 && grecaptcha) {
                                                grecaptcha.reset(_self.get("captcha_id"));
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                });

                /**
                 * Событие нажатия на кнопку отмена
                 * @event clickBtnCancel
                 */
                _self.findComponent("dialogBtnCancel").subscribe("clickBtn", function () {
                    if (_self.get("modal") === true) {
                        _self.hide();
                    }
                });


                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();

                if (_self.getDomInstance().find(".g-recaptcha").length > 0 && grecaptcha) {
                    var _id_captcha = grecaptcha.render($('.g-recaptcha')[0], {
                        sitekey: _self.get("sitekey")
                    });
                    _self.set("captcha_id", _id_captcha);
                }
            }
        }, HttpMixin);
    }
});