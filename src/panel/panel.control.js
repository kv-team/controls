/**
 * @class Panel
 * @classdesc Копмонент панель
 * @extends Control
 *
 * @author A Ivanov
 */
define("controls.panel", [
    "control",
    "controls.button"
], function (Control, Button) {

    return function (options) {

        /**
         * Открытие панели
         */
        Control.prototype.show = function () {
            var panel = this.getDomInstance();
            panel.classList.toggle("kv-hidden");
        };
        /**
         * Закрытие панели
         */
        Control.prototype.hide = function () {

            var panel = this.getDomInstance();
            panel.classList.toggle("kv-hidden");
        };

        return Control.extend({
            template: "#kv-panel-control",
            components: {
                button: Button()
            },
            data: function () {
                return {};
            },
            oninit: function () {
                var _self = this;
                _self._super();
            },
            onrender: function () {
                var _self = this;
                _self._super();
            }
        });
    }
});