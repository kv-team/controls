var TestData = require('./UnitTest/testData');
var TestComponent = require('./UnitTest');
var instancePage = new TestData('<browser name="browserName">' +
    '<accordion name="accordionName"></accordion>' +
    '<breadcrumbs name="breadcrumbsName"></breadcrumbs>' +
    '<button name="buttonName"></button>' +
    '<carousel name="carouselName"><slide name="slideName"></slide></carousel>' +
    '<checkBox name="chechBoxName"></checkBox>' +
    '<dataGrid name="dataGridName"></dataGrid>' +
    '<dialog name="dialogName"></dialog>' +
    '<exportsBtn name="exportsBtnName"></exportsBtn>' +
    '<fieldBtnControl name="fieldBtnControlName"></fieldBtnControl>' +
    '<fieldControl name="fieldControlName"></fieldControl>' +
    '<frame name="frameName"/>' +
    '<image name="imageName"></image>' +
    '<label name="labelName"></label>' +
    '<linkControl name="linkControlName"></linkControl>' +
    '<loadPanel name="loadPanelName"></loadPanel>' +
    '<lookup name="lookupName"></lookup>' +
    '<memo name="memoName"></memo>' +
    '<menu name="menuName"></menu>' +
    '<panel name="panelName"></panel>' +
    '<radio name="radioName"></radio>' +
    '<scrollBtn name="scrollBtnName"></scrollBtn>' +
    '<social name="socialName"></social>' +
    '<tabs name="tabsName"><tab name="tabName"></tab></tabs>' +
    '<toolbar name="toolbarName"></toolbar>' +
    '</browser>');

new TestComponent('browser', instancePage);
new TestComponent('accordion', instancePage);
new TestComponent('breadcrumbs', instancePage);
new TestComponent('button', instancePage);
new TestComponent('carousel', instancePage);
new TestComponent('checkBox', instancePage);
new TestComponent('dataGrid', instancePage);
new TestComponent('dialog', instancePage);
new TestComponent('exportsBtn', instancePage);
new TestComponent('fieldBtnControl', instancePage);
new TestComponent('fieldControl', instancePage);
new TestComponent('frame', instancePage);
new TestComponent('image', instancePage);
new TestComponent('label', instancePage);
new TestComponent('linkControl', instancePage);
new TestComponent('loadPanel', instancePage);
new TestComponent('memo', instancePage);
new TestComponent('menu', instancePage);
new TestComponent('panel', instancePage);
new TestComponent('radio', instancePage);
new TestComponent('scrollBtn', instancePage);
new TestComponent('social', instancePage);
new TestComponent('tabs', instancePage);
new TestComponent('tab', instancePage);
new TestComponent('slide', instancePage);
new TestComponent('toolbar', instancePage);
