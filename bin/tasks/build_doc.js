/**
 * Данные задачи отвечают за сборку документации у проекта
 *
 * @author Иванов Александр Сергеевич
 * @date 21 марта 2017
 */

var fs = require("fs"),
    path = require("path"),
    gulp = require("gulp"),
    rename = require("gulp-rename"),
    gulpsync = require("gulp-sync")(gulp),
    jsdoc = require("gulp-jsdoc-to-markdown"),
    errors = require("../errors"),
    config = require("../../config"),
    clean = require("gulp-clean"),
    concat = require("gulp-concat"),
    watch = require("gulp-watch");

// TODO недостаточно быстро идет обновление документации в проекте. Обдумать на тему обработки по файлу при watch.


/**
 * Сборка документации
 */
gulp.task("build:docs", gulpsync.sync(["clean:docs", "compile:docs"]), function () {
});

/**
 * Удаляем документацию
 */
gulp.task("clean:docs", function () {

    if (fs.existsSync(config.dirDocs())) {

        config.taskStart("clean:docs");

        return gulp.src(config.dirDocs())
            .pipe(clean({force: true})
                .on("data", function () {
                    config.blockOk("Clear file")
                })
                .on("error", errors.ClearError)
            )
            .on("finish", function () {
                config.taskEnd("clean:docs")
            });
    }
});

/**
 * Компилируем документацию
 */
gulp.task("compile:docs", function () {

    config.taskStart("compile:docs");

    return gulp.src(path.join(config.dirSrc(), "**/*.control.js"))
        .pipe(jsdoc()
            .on("data", function () {
                config.blockOk("Compile DOC")
            })
            .on("error", errors.JsDocError))
        .pipe(rename(function (path) {
            path.extname = ".md"
        })
            .on("data", function () {
                config.blockOk("Rename JS => MD")
            })
            .on("error", errors.RenameError))
        .pipe(gulp.dest(config.dirDocs()))
        .pipe(concat("README.md")
            .on("data", function () {
                config.blockOk("Concat MD")
            })
            .on("error", errors.ConcatJsError))
        .pipe(gulp.dest("../"))
        .on("finish", function () {
            config.taskEnd("compile:docs")
        });
});

/**
 * Слушатель который модифицирует документацию
 */
gulp.task("watch:docs", function () {

    config.watchStart("watch:docs");

    watch(path.join(config.dirSrc(), "/**/*.js"), ["build:docs"], function () {

    })
});
